package com.telerikacademy.web.virtualteacher.services.classes;

import com.telerikacademy.web.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.web.virtualteacher.models.StatusesEntity;
import com.telerikacademy.web.virtualteacher.models.TopicsEntity;
import com.telerikacademy.web.virtualteacher.repositories.StatusRepository;
import com.telerikacademy.web.virtualteacher.repositories.TopicRepository;
import com.telerikacademy.web.virtualteacher.services.interfaces.StatusService;
import com.telerikacademy.web.virtualteacher.services.interfaces.TopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StatusServiceImpl implements StatusService {

    private final StatusRepository repository;

    @Autowired
    public StatusServiceImpl(StatusRepository repository) {
        this.repository = repository;
    }


    @Override
    public List<StatusesEntity> getAll() {
        return repository.findAll();
    }

    @Override
    public StatusesEntity getById(int id) {
        Optional<StatusesEntity> optional = repository.findById(id);
        StatusesEntity status = null;
        if (optional.isPresent()) {
            status = optional.get();
        } else {
            throw new EntityNotFoundException("Status", "id", String.valueOf(id));
        }
        return status;
    }

}
