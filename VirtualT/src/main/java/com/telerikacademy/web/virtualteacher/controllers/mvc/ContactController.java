package com.telerikacademy.web.virtualteacher.controllers.mvc;

import com.telerikacademy.web.virtualteacher.config.AuthenticationHelper;
import com.telerikacademy.web.virtualteacher.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/contact")
public class ContactController extends BaseClass {

    @Autowired
    public ContactController(AuthenticationHelper authenticationHelper, UserService userService) {
        super(authenticationHelper, userService);
    }

    @GetMapping
    public String showContactUsPage() {
        return "contact";
    }
}
