package com.telerikacademy.web.virtualteacher.repositories;

import com.telerikacademy.web.virtualteacher.models.AssignmentsEntity;
import com.telerikacademy.web.virtualteacher.models.CoursesEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AssignmentRepository extends JpaRepository<AssignmentsEntity, Integer> {

    List<AssignmentsEntity> getAllByUserId(int id);

    @Query("select a from AssignmentsEntity a where a.user.email like %?1% or a.user.firstName like %?1% or a.user.lastName like %?1%")
    Page<AssignmentsEntity> searchAll(String search, Pageable pageable);

    @Query("select a from AssignmentsEntity a where a.user.id = ?1 and a.lecture.id = ?2")
    List<AssignmentsEntity> getByUserIdAndLectureId(int userId, int lectureId);

    @Query(value = "select sum(grade_id) from virtualteacher.assignments join virtualteacher.lectures l on l.id = assignments.lecture_id where course_id = :courseId and user_id = :userId", nativeQuery = true)
    Double getSumGradeByUserIdAndCourseId(@Param("userId") int userId, @Param("courseId") int courseId);
}