package com.telerikacademy.web.virtualteacher.repositories;

import com.telerikacademy.web.virtualteacher.models.TopicsEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface TopicRepository extends JpaRepository<TopicsEntity, Integer> {
}