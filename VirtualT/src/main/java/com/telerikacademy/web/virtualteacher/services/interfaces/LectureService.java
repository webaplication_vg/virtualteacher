package com.telerikacademy.web.virtualteacher.services.interfaces;

import com.telerikacademy.web.virtualteacher.models.LecturesEntity;
import com.telerikacademy.web.virtualteacher.models.UsersEntity;

import java.util.List;
import java.util.Optional;

public interface LectureService {

	List<LecturesEntity> getAll();

	LecturesEntity getById(int id);

	List<LecturesEntity> searchAll(Optional<String> search, int id);

	void deleteById(UsersEntity initiator, int id);

	void saveLecture(UsersEntity initiator, LecturesEntity lecture);
}
