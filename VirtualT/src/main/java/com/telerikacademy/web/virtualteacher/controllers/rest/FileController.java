package com.telerikacademy.web.virtualteacher.controllers.rest;

import com.telerikacademy.web.virtualteacher.services.interfaces.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
public class FileController {
    private final FileService service;

    @Autowired
    public FileController(FileService service) {
        this.service = service;
    }

    @PostMapping
    public void uploadFile(@RequestParam("file") MultipartFile file) throws IOException {
        service.uploadFile(file);
    }
}
