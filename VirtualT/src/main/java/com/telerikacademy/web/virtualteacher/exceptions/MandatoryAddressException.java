package com.telerikacademy.web.virtualteacher.exceptions;

public class MandatoryAddressException extends RuntimeException{

    public MandatoryAddressException(String role) {
        super(String.format("%s should have an address!",role));
    }

}
