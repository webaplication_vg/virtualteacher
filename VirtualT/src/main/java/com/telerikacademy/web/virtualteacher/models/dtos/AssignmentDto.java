package com.telerikacademy.web.virtualteacher.models.dtos;

import com.telerikacademy.web.virtualteacher.models.LecturesEntity;
import com.telerikacademy.web.virtualteacher.models.UsersEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

public class AssignmentDto {

    private MultipartFile file;
    private Integer grade;
    private UsersEntity user;
    private LecturesEntity lecture;

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public UsersEntity getUser() {
        return user;
    }

    public void setUser(UsersEntity user) {
        this.user = user;
    }

    public LecturesEntity getLecture() {
        return lecture;
    }

    public void setLecture(LecturesEntity lecture) {
        this.lecture = lecture;
    }
}
