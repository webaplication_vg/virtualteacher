package com.telerikacademy.web.virtualteacher.services.interfaces;

import com.telerikacademy.web.virtualteacher.models.CoursesEntity;
import com.telerikacademy.web.virtualteacher.models.LecturesEntity;
import com.telerikacademy.web.virtualteacher.models.TopicsEntity;
import com.telerikacademy.web.virtualteacher.models.UsersEntity;

import java.util.List;
import java.util.Optional;

public interface TopicService {

	List<TopicsEntity> getAll();

	TopicsEntity getById(int id);

	void saveTopic(TopicsEntity topic);


}
