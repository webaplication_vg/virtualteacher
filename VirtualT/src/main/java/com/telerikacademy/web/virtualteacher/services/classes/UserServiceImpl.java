package com.telerikacademy.web.virtualteacher.services.classes;

import com.telerikacademy.web.virtualteacher.config.EmailSender;
import com.telerikacademy.web.virtualteacher.exceptions.DuplicateEntityException;
import com.telerikacademy.web.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.web.virtualteacher.exceptions.UnauthorizedOperationException;
import com.telerikacademy.web.virtualteacher.models.UsersEntity;
import com.telerikacademy.web.virtualteacher.models.VerificationsEntity;
import com.telerikacademy.web.virtualteacher.repositories.UserRepository;
import com.telerikacademy.web.virtualteacher.services.interfaces.UserService;
import com.telerikacademy.web.virtualteacher.services.interfaces.VerificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private static final String ACCESS_ERROR_MESSAGE = "Access is denied!";
    private static final int ADMIN_ID = 4;
    private static final int TEACHER_ID = 3;
    private static final int ADMIN_ROLE_ID = 4;

    private final UserRepository repository;
    private final EmailSender sender;
    private final VerificationService verificationService;

    @Autowired
    public UserServiceImpl(UserRepository repository, EmailSender sender, VerificationService verificationService) {
        this.repository = repository;
        this.sender = sender;
        this.verificationService = verificationService;
    }

    @Override
    public List<UsersEntity> getAll() {
        return repository.findAll();
    }

    @Override
    public List<UsersEntity> getAllStudents() {
        return repository.findAllStudents();
    }

    @Override
    public List<UsersEntity> getAllTeachers() {
        return repository.findAllTeachers();
    }

    @Override
    public UsersEntity getById(int id, UsersEntity usersEntity) {
        if (usersEntity.getRole().getId() < TEACHER_ID && usersEntity.getId() != id) {
            throw new UnauthorizedOperationException("Can't reach here");
        }

        Optional<UsersEntity> optional = repository.findById(id);
        UsersEntity user = null;
        if (optional.isPresent()) {
            user = optional.get();
        } else {
            throw new EntityNotFoundException("User", "id", String.valueOf(id));
        }
        return user;
    }

    @Override
    public void saveUser(UsersEntity usersEntity) throws MessagingException {

        if (repository.findAll().contains(repository.getByEmail(usersEntity.getEmail()))) {
            throw new DuplicateEntityException("User", "email", usersEntity.getEmail());
        }

        repository.save(usersEntity);
        int code = generateRandomNumber();
        createVerification(usersEntity, code);
        triggerMail(usersEntity.getEmail(), code);

    }

    @Override
    public void updateUser(UsersEntity initiator, UsersEntity userToUpdate) {
        if (initiator.getId() != userToUpdate.getId() && initiator.getRole().getId() != ADMIN_ROLE_ID) {
            throw new UnauthorizedOperationException(ACCESS_ERROR_MESSAGE);
        }
        repository.save(userToUpdate);
    }

    @Override
    public void deleteById(int id, UsersEntity initiator) {
        if (initiator.getRole().getId() != ADMIN_ID) {
            throw new UnauthorizedOperationException(ACCESS_ERROR_MESSAGE);
        }
        repository.deleteById(id);
    }

    @Override
    public List<UsersEntity> searchAll(Optional<String> search, UsersEntity initiator) {
        if (initiator.getRole().getId() != ADMIN_ID && initiator.getRole().getId() != TEACHER_ID) {
            throw new UnauthorizedOperationException(ACCESS_ERROR_MESSAGE);
        }
        return repository.searchAll(search);
    }

    @Override
    public UsersEntity getByEmail(String email) {
        return repository.getByEmail(email);
    }

    private int generateRandomNumber() {
        return (int) Math.floor(Math.random() * (100000 - 10000 + 1) + 10000);
    }

    private void createVerification(UsersEntity usersEntity, int code) {
        VerificationsEntity verificationsEnt = new VerificationsEntity();
        verificationsEnt.setUser(usersEntity);
        verificationsEnt.setVerificationCode(code);

        verificationService.create(verificationsEnt);
    }

    private void triggerMail(String email, int code) throws MessagingException {

        sender.sendSimpleEmail(email,
                "Your verification code - " + code,
                "Verification code");

    }
}
