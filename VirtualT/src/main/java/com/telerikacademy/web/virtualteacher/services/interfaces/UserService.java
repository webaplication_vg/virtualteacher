package com.telerikacademy.web.virtualteacher.services.interfaces;

import com.telerikacademy.web.virtualteacher.models.UsersEntity;

import javax.mail.MessagingException;
import java.util.List;
import java.util.Optional;

public interface UserService {

    List<UsersEntity> getAll();

    List<UsersEntity> getAllStudents();

    List<UsersEntity> getAllTeachers();

    UsersEntity getById(int id,UsersEntity usersEntity);

	void saveUser(UsersEntity usersEntity) throws MessagingException;

	void updateUser(UsersEntity initiator, UsersEntity userToUpdate);

	void deleteById(int id, UsersEntity initiator);

	List<UsersEntity> searchAll(Optional<String> search, UsersEntity initiator);

	UsersEntity getByEmail(String email);





}
