package com.telerikacademy.web.virtualteacher.services.interfaces;

import com.telerikacademy.web.virtualteacher.models.TeacherApplicantsEntity;
import com.telerikacademy.web.virtualteacher.models.UsersEntity;

import java.util.List;
import java.util.Optional;

public interface TeacherApplicantService {

	List<TeacherApplicantsEntity> getAll();

	TeacherApplicantsEntity getById(int id);

	void saveApplication(TeacherApplicantsEntity teacherApplicantsEntity);

	List<TeacherApplicantsEntity> searchAll(UsersEntity initiator, Optional<String> search);

	void deleteById(UsersEntity usersEntity, int id);


}
