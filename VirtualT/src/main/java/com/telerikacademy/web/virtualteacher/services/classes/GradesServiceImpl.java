package com.telerikacademy.web.virtualteacher.services.classes;

import com.telerikacademy.web.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.web.virtualteacher.models.AssignmentsEntity;
import com.telerikacademy.web.virtualteacher.models.GradesEntity;
import com.telerikacademy.web.virtualteacher.repositories.AssignmentRepository;
import com.telerikacademy.web.virtualteacher.repositories.GradeRepository;
import com.telerikacademy.web.virtualteacher.services.interfaces.AssignmentService;
import com.telerikacademy.web.virtualteacher.services.interfaces.GradesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class GradesServiceImpl implements GradesService {


    private final GradeRepository repository;

    @Autowired
    public GradesServiceImpl(GradeRepository repository) {
        this.repository = repository;
    }


    @Override
    public List<GradesEntity> getAll() {
        return repository.findAll();
    }

    @Override
    public GradesEntity getById(int id) {
        Optional<GradesEntity> optional = repository.findById(id);
        GradesEntity grade = null;
        if (optional.isPresent()) {
            grade = optional.get();
        } else {
            throw new EntityNotFoundException("Grade", "id", String.valueOf(id));
        }
        return grade;
    }


}
