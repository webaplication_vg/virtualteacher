package com.telerikacademy.web.virtualteacher.services.classes;

import com.telerikacademy.web.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.web.virtualteacher.exceptions.UnauthorizedOperationException;
import com.telerikacademy.web.virtualteacher.models.CoursesEntity;
import com.telerikacademy.web.virtualteacher.models.UsersCoursesEntity;
import com.telerikacademy.web.virtualteacher.models.UsersEntity;
import com.telerikacademy.web.virtualteacher.repositories.CourseRepository;
import com.telerikacademy.web.virtualteacher.services.interfaces.AssignmentService;
import com.telerikacademy.web.virtualteacher.services.interfaces.CourseService;
import com.telerikacademy.web.virtualteacher.services.interfaces.UserCourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CourseServiceImpl implements CourseService {

    private static final String ACCESS_ERROR_MESSAGE = "Access is denied!";
    private static final int AUTHORIZED_USER_ID = 3;
    private static final int AVERAGE_GRADE = 3;

    private final CourseRepository repository;
    private final UserCourseService userCourseService;
    private final AssignmentService assignmentService;

    @Autowired
    public CourseServiceImpl(CourseRepository repository, UserCourseService userCourseService, AssignmentService assignmentService) {
        this.repository = repository;
        this.userCourseService = userCourseService;
        this.assignmentService = assignmentService;
    }

    @Override
    public List<CoursesEntity> getAll() {
        return repository.findAll();
    }

    @Override
    public CoursesEntity getById(int id) {
        Optional<CoursesEntity> optional = repository.findById(id);
        CoursesEntity course = null;
        if (optional.isPresent()) {
            course = optional.get();
        } else {
            throw new EntityNotFoundException("Course", "id", String.valueOf(id));
        }
        return course;
    }

    @Override
    public void deleteById(UsersEntity initiator, int id) {
        if (initiator.getRole().getId() < AUTHORIZED_USER_ID) {
            throw new UnauthorizedOperationException(ACCESS_ERROR_MESSAGE);
        }

        repository.deleteById(id);
    }

    @Override
    public void saveCourse(UsersEntity initiator, CoursesEntity course) {
        repository.save(course);
    }

    @Override
    public void setRating(UsersCoursesEntity userCourse, UsersEntity user) {
        if (assignmentService.averageGrade(user.getId(), userCourse.getCourse().getId()) < AVERAGE_GRADE) {
            throw new UnauthorizedOperationException("Not allowed");
        }

        CoursesEntity course = userCourse.getCourse();
        course.setRating(userCourseService.getCourseSumRating(userCourse.getCourse().getId()) / userCourseService.getUserCountByCourseId(userCourse.getCourse().getId()));
        repository.save(course);
    }

    @Override
    public CoursesEntity getCourse(int id) {
        return repository.getCourse(id);
    }

    @Override
    public List<CoursesEntity> getMyCourses(int id) {
        return repository.getMyCourses(id);
    }

    @Override
    public List<CoursesEntity> searchAll(Optional<String> search) {
        return repository.searchAll(search);
    }
}
