package com.telerikacademy.web.virtualteacher.repositories;

import com.telerikacademy.web.virtualteacher.models.AssignmentsEntity;
import com.telerikacademy.web.virtualteacher.models.GradesEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GradeRepository extends JpaRepository<GradesEntity, Integer> {

}