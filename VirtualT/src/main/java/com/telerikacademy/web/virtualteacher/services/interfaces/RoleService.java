package com.telerikacademy.web.virtualteacher.services.interfaces;

import com.telerikacademy.web.virtualteacher.models.RolesEntity;
import com.telerikacademy.web.virtualteacher.models.TopicsEntity;

import java.util.List;

public interface RoleService {

	List<RolesEntity> getAll();

	RolesEntity getById(int id);

}
