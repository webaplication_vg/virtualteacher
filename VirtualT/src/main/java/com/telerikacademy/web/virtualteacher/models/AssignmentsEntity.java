package com.telerikacademy.web.virtualteacher.models;

import org.springframework.web.bind.annotation.GetMapping;

import javax.persistence.*;

@Entity
@Table(name = "assignments", schema = "virtualteacher", catalog = "")
public class AssignmentsEntity {
    private int id;
    private String file;
    private GradesEntity grade;
    private UsersEntity user;
    private LecturesEntity lecture;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    @Basic
    @Column(name = "file", nullable = true, length = 100)
    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }


    @ManyToOne
    @JoinColumn(name = "grade_id", referencedColumnName = "id")
    public GradesEntity getGrade() {
        return grade;
    }

    public void setGrade(GradesEntity gradesByGradeId) {
        this.grade = gradesByGradeId;
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    public UsersEntity getUser() {
        return user;
    }

    public void setUser(UsersEntity usersByUserId) {
        this.user = usersByUserId;
    }

    @ManyToOne
    @JoinColumn(name = "lecture_id", referencedColumnName = "id")
    public LecturesEntity getLecture() {
        return lecture;
    }

    public void setLecture(LecturesEntity lecturesByLectureId) {
        this.lecture = lecturesByLectureId;
    }
}
