package com.telerikacademy.web.virtualteacher.services.interfaces;

import com.telerikacademy.web.virtualteacher.models.CoursesEntity;
import com.telerikacademy.web.virtualteacher.models.UsersCoursesEntity;
import com.telerikacademy.web.virtualteacher.models.UsersEntity;

import java.util.List;
import java.util.Optional;

public interface CourseService {

	List<CoursesEntity> getAll();

	CoursesEntity getById(int id);

	void deleteById(UsersEntity usersEntity,int id);

	void saveCourse(UsersEntity usersEntity, CoursesEntity coursesEntity);

	void setRating(UsersCoursesEntity usersCoursesEntity, UsersEntity user);

	List<CoursesEntity> searchAll(Optional<String> search);

	CoursesEntity getCourse(int id);

	List<CoursesEntity> getMyCourses(int id);

}
