package com.telerikacademy.web.virtualteacher.services.classes;

import com.telerikacademy.web.virtualteacher.exceptions.DuplicateEntityException;
import com.telerikacademy.web.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.web.virtualteacher.exceptions.UnauthorizedOperationException;
import com.telerikacademy.web.virtualteacher.models.TeacherApplicantsEntity;
import com.telerikacademy.web.virtualteacher.models.UsersEntity;
import com.telerikacademy.web.virtualteacher.repositories.TeacherApplicantRepository;
import com.telerikacademy.web.virtualteacher.services.interfaces.TeacherApplicantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TeacherApplicantServiceImpl implements TeacherApplicantService {

    private static final String ACCESS_ERROR_MESSAGE = "Access is denied!";
    private static final int AUTHORIZED_USER_ID = 3;
    public static final int ADMIN_ID = 4;

    private final TeacherApplicantRepository repository;

    @Autowired
    public TeacherApplicantServiceImpl(TeacherApplicantRepository repository) {
        this.repository = repository;
    }


    @Override
    public List<TeacherApplicantsEntity> getAll() {
        return repository.findAll();
    }

    @Override
    public List<TeacherApplicantsEntity> searchAll(UsersEntity initiator, Optional<String> search) {
        if (initiator.getRole().getId() != ADMIN_ID) {
            throw new UnauthorizedOperationException(ACCESS_ERROR_MESSAGE);
        }
        return repository.searchAll(search);
    }

    @Override
    public void deleteById(UsersEntity initiator, int id) {
        if(initiator.getRole().getId() < AUTHORIZED_USER_ID){
            throw new UnauthorizedOperationException(ACCESS_ERROR_MESSAGE);
        }

        repository.deleteById(id);
    }

    @Override
    public TeacherApplicantsEntity getById(int id) {
        Optional<TeacherApplicantsEntity> optional = repository.findById(id);
        TeacherApplicantsEntity applicant = null;
        if (optional.isPresent()) {
            applicant = optional.get();
        } else {
            throw new EntityNotFoundException("Application", "id", String.valueOf(id));
        }
        return applicant;
    }

    @Override
    public void saveApplication(TeacherApplicantsEntity teacherApplicantsEntity) {

        if(!repository.getByUserId(teacherApplicantsEntity.getUser().getId()).isEmpty()){
            throw new DuplicateEntityException("Teacher application", "UserID", String.valueOf(teacherApplicantsEntity.getUser().getId()));
        }

        repository.save(teacherApplicantsEntity);
    }

}
