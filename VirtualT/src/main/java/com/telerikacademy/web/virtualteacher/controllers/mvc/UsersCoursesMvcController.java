package com.telerikacademy.web.virtualteacher.controllers.mvc;

import com.telerikacademy.web.virtualteacher.config.AuthenticationHelper;
import com.telerikacademy.web.virtualteacher.services.interfaces.UserCourseService;
import com.telerikacademy.web.virtualteacher.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.Optional;

@Controller
@RequestMapping("/my-courses")
public class UsersCoursesMvcController extends BaseClass {

    private final UserCourseService userCourseService;


    @Autowired
    public UsersCoursesMvcController(UserCourseService userCourseService, AuthenticationHelper authenticationHelper, UserService userService) {
        super(authenticationHelper, userService);
        this.userCourseService = userCourseService;
    }

    @GetMapping("/{id}")
    public String showUsersPage(@PathVariable int id,
                                @RequestParam(defaultValue = "") Optional<String> search,
                                Model model, HttpSession session) {
        String x = checkIfLogged(session);
        if (x != null) return x;

        model.addAttribute("courses", userCourseService.searchAll(search, id));
        return "my-courses";
    }

}
