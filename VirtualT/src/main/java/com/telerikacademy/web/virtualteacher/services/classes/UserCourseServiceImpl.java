package com.telerikacademy.web.virtualteacher.services.classes;

import com.telerikacademy.web.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.web.virtualteacher.exceptions.UnauthorizedOperationException;
import com.telerikacademy.web.virtualteacher.models.CoursesEntity;
import com.telerikacademy.web.virtualteacher.models.UsersCoursesEntity;
import com.telerikacademy.web.virtualteacher.models.UsersEntity;
import com.telerikacademy.web.virtualteacher.repositories.UserCourseRepository;
import com.telerikacademy.web.virtualteacher.services.interfaces.UserCourseService;
import com.telerikacademy.web.virtualteacher.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserCourseServiceImpl implements UserCourseService {

    private static final int UNAUTHORISED_USER_ID = 2;
    private final UserCourseRepository repository;
    private final UserService userService;

    @Autowired
    public UserCourseServiceImpl(UserCourseRepository repository, UserService userService) {
        this.repository = repository;
        this.userService = userService;
    }


    @Override
    public List<UsersCoursesEntity> getAll() {
        return repository.findAll();
    }

    @Override
    public UsersCoursesEntity getById(int id) {
        Optional<UsersCoursesEntity> optional = repository.findById(id);
        UsersCoursesEntity userCourse = null;
        if (optional.isPresent()) {
            userCourse = optional.get();
        } else {
            throw new EntityNotFoundException("UserCourse", "id", String.valueOf(id));
        }
        return userCourse;
    }

    @Override
    public UsersCoursesEntity getByCourseId(int courseId, UsersEntity user) {
        if (repository.getByCourseId(courseId, user.getId()) != null || userService.getById(user.getId(),user).getRole().getId() > UNAUTHORISED_USER_ID)
            return repository.getByCourseId(courseId, user.getId());
        else
            throw new UnauthorizedOperationException("Can't reach here!");
    }

    @Override
    public void saveUserCourse(UsersCoursesEntity userCourse) {
        repository.save(userCourse);
    }

    @Override
    public List<UsersCoursesEntity> searchAll(Optional<String> search, int id) {
        return repository.searchAll(search, id);
    }

    @Override
    public Double getCourseSumRating(int courseId) {
        return repository.getCourseSumRating(courseId);
    }

    @Override
    public Double getUserCountByCourseId(int courseId) {
        return repository.getUserCountByCourseId(courseId);
    }
}
