package com.telerikacademy.web.virtualteacher.controllers.mvc;

import com.telerikacademy.web.virtualteacher.config.AuthenticationHelper;
import com.telerikacademy.web.virtualteacher.exceptions.AuthenticationFailureException;
import com.telerikacademy.web.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.web.virtualteacher.exceptions.UnauthorizedOperationException;
import com.telerikacademy.web.virtualteacher.models.UsersEntity;
import com.telerikacademy.web.virtualteacher.models.dtos.MultipartFileDto;
import com.telerikacademy.web.virtualteacher.services.interfaces.FileService;
import com.telerikacademy.web.virtualteacher.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Optional;

@Controller
@RequestMapping("/users")
public class UsersMvcController extends BaseClass {

    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;
    private final FileService fileService;

    @Autowired
    public UsersMvcController(UserService userService, AuthenticationHelper authenticationHelper, FileService fileService) {
        super(authenticationHelper, userService);
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
        this.fileService = fileService;
    }

    @GetMapping
    public String showUsersPage(HttpSession session,
                                @RequestParam(defaultValue = "") Optional<String> search,
                                Model model) {
        String x = checkIfLogged(session);
        if (x != null) return x;

        try {
            UsersEntity initiator = authenticationHelper.tryGetUser(session);
            model.addAttribute(USERS, userService.searchAll(search, initiator));
            return USERS;
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return ACCESS_DENIED;
        }
    }

    @GetMapping("/{id}")
    public String showUserProfile(@PathVariable int id, Model model, HttpSession session) {

        String x = checkIfLogged(session);
        if (x != null) return x;

        try {
            model.addAttribute("user", userService.getById(id, authenticationHelper.tryGetUser(session)));
            return PROFILE;
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return NOT_FOUND;
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return ACCESS_DENIED;
        }
    }

    @GetMapping("/update/{id}")
    public String showEditUserPage(@PathVariable int id, Model model, HttpSession session) {

        String x = checkIfLogged(session);
        if (x != null) return x;

        try {
            authenticationHelper.tryGetUser(session);
            model.addAttribute("updatedUser", userService.getById(id, authenticationHelper.tryGetUser(session)));
            model.addAttribute("file", new MultipartFileDto());
            return PROFILE_EDIT;
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return NOT_FOUND;
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return ACCESS_DENIED;
        }
    }

    @PostMapping("/update/{id}")
    public String editUser(@PathVariable int id,
                           @Valid UsersEntity user,
                           BindingResult errors,
                           Model model,
                           HttpSession session) {
        if (errors.hasErrors()) {
            return PROFILE_EDIT;
        }
        try {
            UsersEntity initiator = authenticationHelper.tryGetUser(session);
            user.setId(id);
            userService.updateUser(initiator, user);
            return "redirect:/users/" + id;
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return NOT_FOUND;
        } catch (UnauthorizedOperationException | AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return ACCESS_DENIED;
        }
    }

    @PostMapping("/update-picture/{id}")
    public String changePicture(@PathVariable int id,
                                MultipartFileDto multipartFileDto,
                                BindingResult errors,
                                Model model,
                                HttpSession session) {
        if (errors.hasErrors()) {
            return PROFILE_EDIT;
        }
        try {
            fileService.uploadFile(multipartFileDto.getFile());
            UsersEntity usersEntity = userService.getById(id, authenticationHelper.tryGetUser(session));
            usersEntity.setProfilePicture(multipartFileDto.getFile().getOriginalFilename());
            userService.updateUser(userService.getById(id, authenticationHelper.tryGetUser(session)), usersEntity);
            return "redirect:/users/" + id;
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return NOT_FOUND;
        } catch (UnauthorizedOperationException | AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return ACCESS_DENIED;
        } catch (IOException e) {
            return NOT_FOUND;
        }
    }
}
