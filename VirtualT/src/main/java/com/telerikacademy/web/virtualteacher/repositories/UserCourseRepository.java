package com.telerikacademy.web.virtualteacher.repositories;

import com.telerikacademy.web.virtualteacher.models.UsersCoursesEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserCourseRepository extends JpaRepository<UsersCoursesEntity, Integer> {

    @Query(value = "select * from virtualteacher.users_courses join virtualteacher.courses c on c.id = users_courses.course_id join virtualteacher.topics t on t.id = c.topic_id join virtualteacher.users u on u.id = users_courses.user_id where u.id=:id and ((:search='' or c.name like concat('%',:search,'%')) or (:search='' or t.name like concat('%',:search,'%')))", nativeQuery = true)
    List<UsersCoursesEntity> searchAll(@Param("search") Optional<String> search, @Param("id") int id);

    @Query(value = "select * from virtualteacher.users_courses join virtualteacher.courses c on c.id = users_courses.course_id where c.id = :courseId and user_id = :userId ", nativeQuery = true)
    UsersCoursesEntity getByCourseId(@Param("courseId") int courseId, @Param("userId") int userId);

    @Query(value = "select sum(course_rating) from virtualteacher.users_courses join virtualteacher.courses c on c.id = users_courses.course_id where c.id = :courseId", nativeQuery = true)
    Double getCourseSumRating(@Param("courseId") int courseId);

    @Query(value = "select count(user_id) from virtualteacher.users_courses join virtualteacher.courses c on c.id = users_courses.course_id where c.id = :courseId and course_rating!=0", nativeQuery = true)
    Double getUserCountByCourseId(@Param("courseId") int courseId);

}