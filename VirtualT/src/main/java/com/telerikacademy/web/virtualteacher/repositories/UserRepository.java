package com.telerikacademy.web.virtualteacher.repositories;

import com.telerikacademy.web.virtualteacher.models.UsersEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<UsersEntity, Integer> {

    @Query(value = "select * from virtualteacher.users where role_id!=4 and (first_name like concat('%',:search,'%') or last_name like concat('%',:search,'%') or email like concat('%',:search,'%'))", nativeQuery = true)
    List<UsersEntity> searchAll(@Param("search") Optional<String> search);

    @Query(value = "select * from virtualteacher.users where role_id = 3", nativeQuery = true)
    List<UsersEntity> findAllTeachers();

    @Query(value = "select * from virtualteacher.users where role_id = 2", nativeQuery = true)
    List<UsersEntity> findAllStudents();

    @Query(value = "select * from virtualteacher.users where email = :email", nativeQuery = true)
    UsersEntity getByEmail(@Param("email") String email);

}