package com.telerikacademy.web.virtualteacher.repositories;

import com.telerikacademy.web.virtualteacher.models.CoursesEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CourseRepository extends JpaRepository<CoursesEntity, Integer> {

    @Query(value = "select * from virtualteacher.courses join virtualteacher.topics t on t.id = courses.topic_id where (:search='' or courses.name like concat('%',:search,'%')) or (:search='' or t.name like concat('%',:search,'%'))",nativeQuery = true)
    List<CoursesEntity> searchAll(@Param("search") Optional<String> search);

    @Query(value = "select * from virtualteacher.courses where id=:id", nativeQuery = true)
    CoursesEntity getCourse(@Param("id")int id);

    @Query(value = "select c.id, name, picture, description, starting_date, topic_id, status_id,rating from virtualteacher.users_courses join virtualteacher.courses c on c.id = users_courses.course_id where :id=user_id", nativeQuery = true)
    List<CoursesEntity> getMyCourses(@Param("id")int id);

}