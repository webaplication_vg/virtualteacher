package com.telerikacademy.web.virtualteacher.services.classes;

import com.telerikacademy.web.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.web.virtualteacher.exceptions.UnauthorizedOperationException;
import com.telerikacademy.web.virtualteacher.models.AssignmentsEntity;
import com.telerikacademy.web.virtualteacher.models.UsersEntity;
import com.telerikacademy.web.virtualteacher.repositories.AssignmentRepository;
import com.telerikacademy.web.virtualteacher.repositories.LectureRepository;
import com.telerikacademy.web.virtualteacher.services.interfaces.AssignmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AssignmentServiceImpl implements AssignmentService {
    private static final String ACCESS_ERROR_MESSAGE = "Access is denied!";
    private static final int AUTHORIZED_STUDENT = 2;
    private static final int AUTHORIZED_TEACHER = 3;
    private static final int NO_GRADES = 0;

    private final AssignmentRepository repository;
    private final LectureRepository lectureRepository;

    @Autowired
    public AssignmentServiceImpl(AssignmentRepository repository, LectureRepository lectureRepository) {
        this.repository = repository;
        this.lectureRepository = lectureRepository;
    }


    @Override
    public List<AssignmentsEntity> getAll() {
        return repository.findAll();
    }

    @Override
    public List<AssignmentsEntity> getAllByUserId(int id) {
        return repository.getAllByUserId(id);
    }

    @Override
    public List<AssignmentsEntity> getByUserIdAndLectureId(int userId, int lectureId) {
        return repository.getByUserIdAndLectureId(userId, lectureId);
    }

    @Override
    public AssignmentsEntity getById(int id) {
        Optional<AssignmentsEntity> optional = repository.findById(id);
        AssignmentsEntity assignment = null;
        if (optional.isPresent()) {
            assignment = optional.get();
        } else {
            throw new EntityNotFoundException("Assignment", "id", String.valueOf(id));
        }
        return assignment;
    }

    @Override
    public double averageGrade(int userId, int courseId) {
        if (repository.getSumGradeByUserIdAndCourseId(userId, courseId) == null || lectureRepository.getLecturesNumberByCourseId(courseId) == null) {
            return NO_GRADES;
        }
        return repository.getSumGradeByUserIdAndCourseId(userId, courseId) / lectureRepository.getLecturesNumberByCourseId(courseId);
    }

    @Override
    public void saveAssignment(UsersEntity initiator, AssignmentsEntity assignmentsEntity) {
        if (initiator.getRole().getId() < AUTHORIZED_STUDENT) {
            throw new UnauthorizedOperationException(ACCESS_ERROR_MESSAGE);
        }
        repository.save(assignmentsEntity);
    }

    @Override
    public Page<AssignmentsEntity> findPaginated(int pageNo, int pageSize, String sortField, String sortDirection, String search) {
        Sort sort = sortDirection.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortField).ascending() : Sort.by(sortField).descending();

        Pageable pageable = PageRequest.of(pageNo - 1, pageSize, sort);
        return repository.searchAll(search, pageable);
    }
}
