package com.telerikacademy.web.virtualteacher.exceptions;

import java.util.Optional;

public class BadOperationException extends RuntimeException{

    public BadOperationException(String text) {
        super(text);
    }

}
