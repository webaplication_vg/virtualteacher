package com.telerikacademy.web.virtualteacher.controllers.rest;


import com.telerikacademy.web.virtualteacher.config.AuthenticationHelper;
import com.telerikacademy.web.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.web.virtualteacher.exceptions.UnauthorizedOperationException;
import com.telerikacademy.web.virtualteacher.models.UsersCoursesEntity;
import com.telerikacademy.web.virtualteacher.models.UsersEntity;
import com.telerikacademy.web.virtualteacher.services.interfaces.UserCourseService;
import com.telerikacademy.web.virtualteacher.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.mail.MessagingException;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/users")
public class UserController {

    private final UserService service;
    private final AuthenticationHelper authenticationHelper;
    private final UserCourseService userCourseService;

    @Autowired
    public UserController(UserService service, AuthenticationHelper authenticationHelper, UserCourseService userCourseService) {
        this.service = service;
        this.authenticationHelper = authenticationHelper;
        this.userCourseService = userCourseService;
    }

    @GetMapping()
    public List<UsersEntity> getAll() {
        return service.getAll();
    }

    @GetMapping("/students")
    public List<UsersEntity> getAllStudents() {
        return service.getAllStudents();
    }

    @GetMapping("/teachers")
    public List<UsersEntity> getAllTeachers() {
        return service.getAllTeachers();
    }

    @GetMapping("/search")
    public List<UsersEntity> search(@RequestHeader HttpHeaders headers, @RequestParam(required = false) Optional<String> word) {
        try {
            UsersEntity user = authenticationHelper.tryGetUser(headers);
            return service.searchAll(word, user);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public UsersEntity getById(@PathVariable int id, @RequestHeader HttpHeaders headers) {
        try {
            return service.getById(id, authenticationHelper.tryGetUser(headers));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public void create(@Valid @RequestBody UsersEntity usersEntity) throws MessagingException {
        service.saveUser(usersEntity);
    }

    @PutMapping("/{id}")
    public void update(@PathVariable int id, @RequestHeader HttpHeaders headers, @Valid @RequestBody UsersEntity usersEntity) {
        try {
            UsersEntity user = authenticationHelper.tryGetUser(headers);
            usersEntity.setId(id);
            service.updateUser(user, usersEntity);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers,
                       @PathVariable int id) {
        try {
            UsersEntity user = authenticationHelper.tryGetUser(headers);
            service.deleteById(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/{id}/my-courses")
    public List<UsersCoursesEntity> showMyCourses(@PathVariable int id,
                                                  @RequestParam(defaultValue = "", required = false) Optional<String> search,
                                                  @RequestHeader HttpHeaders headers) {
        try {
            authenticationHelper.tryGetUser(headers);
            return userCourseService.searchAll(search, id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}