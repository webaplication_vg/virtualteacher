package com.telerikacademy.web.virtualteacher.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "verifications", schema = "virtualteacher", catalog = "")
public class VerificationsEntity {
    private int id;
    private int verificationCode;
    private UsersEntity user;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    @Basic
    @Column(name = "verification_code", nullable = false)
    public int getVerificationCode() {
        return verificationCode;
    }

    public void setVerificationCode(int verificationCode) {
        this.verificationCode = verificationCode;
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    public UsersEntity getUser() {
        return user;
    }

    public void setUser(UsersEntity usersByUserId) {
        this.user = usersByUserId;
    }
}
