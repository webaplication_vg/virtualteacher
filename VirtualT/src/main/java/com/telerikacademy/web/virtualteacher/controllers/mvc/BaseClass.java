package com.telerikacademy.web.virtualteacher.controllers.mvc;


import com.telerikacademy.web.virtualteacher.config.AuthenticationHelper;
import com.telerikacademy.web.virtualteacher.exceptions.AuthenticationFailureException;
import com.telerikacademy.web.virtualteacher.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.servlet.http.HttpSession;

@Component
public abstract class BaseClass {


    static final String ACCESS_DENIED = "access-denied";
    static final String NOT_FOUND = "not-found";
    static final String ASSIGNMENT_GRADE = "assignment-grade";
    static final String ASSIGNMENTS_TABLE = "assignments-table";
    static final String COURSE_EDIT = "course-edit";
    static final String LOGIN = "login";
    static final String REGISTER = "register";
    static final String VERIFICATION = "verification";
    static final String COURSE_NEW = "course-new";
    static final String LECTURES = "lectures";
    static final String LECTURE_NEW = "lecture-new";
    static final String LECTURE_EDIT = "lecture-edit";
    static final String PROFILE_EDIT = "profile-edit";
    static final String PROFILE = "profile";
    static final String USERS = "users";

    private final AuthenticationHelper authenticationHelper;
    private final UserService userService;


    @Autowired
    public BaseClass(AuthenticationHelper authenticationHelper, UserService userService) {
        this.authenticationHelper = authenticationHelper;
        this.userService = userService;
    }

    @ModelAttribute("currentUser")
    public String getCurrentUser(HttpSession session) {
        if (session.getAttribute("currentUser") != null) {
            return authenticationHelper.tryGetUser(session).getEmail();
        }
        return null;
    }

    @ModelAttribute("currentUserId")
    public int getCurrentUserId(HttpSession session) {
        if (session.getAttribute("currentUser") != null) {
            return authenticationHelper.tryGetUser(session).getId();
        }
        return -1;
    }

    @ModelAttribute("profilePicture")
    public String userProfilePicture(HttpSession session) {
        if (session.getAttribute("currentUser") != null) {
            return authenticationHelper.tryGetUser(session).getProfilePicture();
        }
        return null;
    }

    @ModelAttribute("names")
    public String userProfileNames(HttpSession session) {
        if (session.getAttribute("currentUser") != null) {
            return authenticationHelper.tryGetUser(session).getFirstName() + " " + authenticationHelper.tryGetUser(session).getLastName();
        }
        return null;
    }

    @ModelAttribute("isNotVerified")
    public boolean isNotVerified(HttpSession session) {
        return checkRole(session, 1);
    }

    @ModelAttribute("isStudent")
    public boolean isStudent(HttpSession session) {
        return checkRole(session, 2);
    }

    @ModelAttribute("isTeacher")
    public boolean isTeacher(HttpSession session) {
        return checkRole(session, 3);
    }

    @ModelAttribute("isAdmin")
    public boolean isAdmin(HttpSession session) {
        return checkRole(session, 4);
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }


    @ModelAttribute("myId")
    public Integer showMyProfile(HttpSession session) {
        if (session.getAttribute("currentUser") != null) {
            return authenticationHelper.tryGetUser(session).getId();
        }
        return null;
    }


    private boolean checkRole(HttpSession session, int roleId) {
        String currentUser;
        if (session.getAttribute("currentUser") != null) {
            currentUser = (String) session.getAttribute("currentUser");
            return userService.getByEmail(currentUser).getRole().getId() == roleId;
        }
        return session.getAttribute("currentUser") != null;
    }

    public String checkIfLogged(HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "access-denied";
        }
        return null;
    }
}
