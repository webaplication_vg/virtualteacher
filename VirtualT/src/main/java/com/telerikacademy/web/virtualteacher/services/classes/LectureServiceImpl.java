package com.telerikacademy.web.virtualteacher.services.classes;

import com.telerikacademy.web.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.web.virtualteacher.exceptions.UnauthorizedOperationException;
import com.telerikacademy.web.virtualteacher.models.LecturesEntity;
import com.telerikacademy.web.virtualteacher.models.UsersEntity;
import com.telerikacademy.web.virtualteacher.repositories.LectureRepository;
import com.telerikacademy.web.virtualteacher.services.interfaces.LectureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class LectureServiceImpl implements LectureService {

    private static final String ACCESS_ERROR_MESSAGE = "Access is denied!";
    private static final int TEACHER_ID = 3;

    private final LectureRepository repository;

    @Autowired
    public LectureServiceImpl(LectureRepository repository) {
        this.repository = repository;
    }


    @Override
    public List<LecturesEntity> getAll() {
        return repository.findAll();
    }

    @Override
    public LecturesEntity getById(int id) {
        Optional<LecturesEntity> optional = repository.findById(id);
        LecturesEntity lecture = null;
        if (optional.isPresent()) {
            lecture = optional.get();
        } else {
            throw new EntityNotFoundException("Lecture", "id", String.valueOf(id));
        }
        return lecture;
    }

    @Override
    public List<LecturesEntity> searchAll(Optional<String> search, int id) {
        return repository.searchAll(search, id);
    }

    @Override
    public void saveLecture(UsersEntity initiator, LecturesEntity lecture) {
        if (initiator.getRole().getId() < TEACHER_ID) {
            throw new UnauthorizedOperationException(ACCESS_ERROR_MESSAGE);
        }

        repository.save(lecture);
    }

    @Override
    public void deleteById(UsersEntity initiator, int id) {
        if (initiator.getRole().getId() < TEACHER_ID) {
            throw new UnauthorizedOperationException(ACCESS_ERROR_MESSAGE);
        }

        repository.deleteById(id);
    }
}
