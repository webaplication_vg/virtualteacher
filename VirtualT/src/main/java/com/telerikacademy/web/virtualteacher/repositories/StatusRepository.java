package com.telerikacademy.web.virtualteacher.repositories;

import com.telerikacademy.web.virtualteacher.models.StatusesEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface StatusRepository extends JpaRepository<StatusesEntity, Integer> {
}