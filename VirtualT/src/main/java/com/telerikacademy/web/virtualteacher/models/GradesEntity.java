package com.telerikacademy.web.virtualteacher.models;

import javax.persistence.*;

@Entity
@Table(name = "grades", schema = "virtualteacher", catalog = "")
public class GradesEntity {
    private int id;
    private String name;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = true, length = 50)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
