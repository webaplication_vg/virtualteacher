package com.telerikacademy.web.virtualteacher.repositories;

import com.telerikacademy.web.virtualteacher.models.LecturesEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface LectureRepository extends JpaRepository<LecturesEntity, Integer> {

    @Query(value = "select * from virtualteacher.lectures where lectures.course_id = :id and (:search='' or lectures.name like concat('%',:search,'%'))", nativeQuery = true)
    List<LecturesEntity> searchAll(@Param("search") Optional<String> search,@Param("id") int id);

    @Query(value = "select count(*) from virtualteacher.lectures where course_id=:courseId", nativeQuery = true)
    Double getLecturesNumberByCourseId(@Param("courseId") int courseId);
}