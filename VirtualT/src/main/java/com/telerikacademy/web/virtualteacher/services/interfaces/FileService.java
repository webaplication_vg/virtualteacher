package com.telerikacademy.web.virtualteacher.services.interfaces;

import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

public interface FileService {
    void uploadFile(MultipartFile file) throws IOException;

     String getFile(String path) throws IOException;
}
