package com.telerikacademy.web.virtualteacher.services.classes;

import com.telerikacademy.web.virtualteacher.services.interfaces.FileService;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

@Service
public class FileServiceImpl implements FileService {
    @Override
    public void uploadFile(MultipartFile file) throws IOException {
        file.transferTo(new File(System.getProperty("user.dir") + "\\src\\main\\resources\\static\\files\\" + file.getOriginalFilename()));
    }

    @Override
    public String getFile(String path) throws IOException {
        File file = new File(System.getProperty("user.dir") + "\\src\\main\\resources\\static\\files\\" + path);
        Scanner sc = new Scanner(file);
        StringBuilder str = new StringBuilder();

        while (sc.hasNextLine()) {
            str.append(sc.nextLine()).append("\n");
        }

        return str.toString();
    }
}
