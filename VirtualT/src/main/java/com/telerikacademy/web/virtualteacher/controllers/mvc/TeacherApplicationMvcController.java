package com.telerikacademy.web.virtualteacher.controllers.mvc;

import com.telerikacademy.web.virtualteacher.config.AuthenticationHelper;
import com.telerikacademy.web.virtualteacher.exceptions.AuthenticationFailureException;
import com.telerikacademy.web.virtualteacher.exceptions.DuplicateEntityException;
import com.telerikacademy.web.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.web.virtualteacher.exceptions.UnauthorizedOperationException;
import com.telerikacademy.web.virtualteacher.models.TeacherApplicantsEntity;
import com.telerikacademy.web.virtualteacher.models.UsersEntity;
import com.telerikacademy.web.virtualteacher.services.interfaces.RoleService;
import com.telerikacademy.web.virtualteacher.services.interfaces.TeacherApplicantService;
import com.telerikacademy.web.virtualteacher.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.Optional;

@Controller
@RequestMapping("/application")
public class TeacherApplicationMvcController extends BaseClass {

    private final TeacherApplicantService teacherApplicantService;
    private final RoleService roleService;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public TeacherApplicationMvcController(TeacherApplicantService teacherApplicantService, RoleService roleService, AuthenticationHelper authenticationHelper, UserService userService) {
        super(authenticationHelper, userService);
        this.teacherApplicantService = teacherApplicantService;
        this.roleService = roleService;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public String showTeacherApplicationPage(Model model, HttpSession session) {
        String x = checkIfLogged(session);
        if (x != null) return x;

        model.addAttribute("teacherApplication", new TeacherApplicantsEntity());
        return "teacher-application";
    }

    @PostMapping("/save")
    public String saveApplication(@ModelAttribute("application") TeacherApplicantsEntity application,
                                  Model model,
                                  HttpSession session) {
        try {
            application.setUser(authenticationHelper.tryGetUser(session));
            teacherApplicantService.saveApplication(application);
            return "redirect:/";
        } catch (UnauthorizedOperationException | AuthenticationFailureException | DuplicateEntityException e) {
            model.addAttribute("error", e.getMessage());
            return "redirect:/";
        }
    }

    @GetMapping("/users")
    public String showTeacherApplicantsPage(@RequestParam(defaultValue = "") Optional<String> search,
                                            Model model,
                                            HttpSession session) {
        try {
            UsersEntity initiator = authenticationHelper.tryGetUser(session);
            model.addAttribute("applicants", teacherApplicantService.searchAll(initiator, search));
            return "teacher-applicants";
        } catch (UnauthorizedOperationException | AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return ACCESS_DENIED;
        }
    }

    @GetMapping("/delete/{id}")
    public String deleteTeacherApplicant(@PathVariable int id, Model model, HttpSession session) {
        try {
            teacherApplicantService.deleteById(authenticationHelper.tryGetUser(session), id);
            return "redirect:/application/users";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return NOT_FOUND;
        } catch (UnauthorizedOperationException | AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return ACCESS_DENIED;
        }
    }

    @GetMapping("/update/{id}")
    public String updateTeacherApplicant(@PathVariable int id, Model model, HttpSession session) {
        try {
            teacherApplicantService.getById(id).getUser().setRole(roleService.getById(3));
            teacherApplicantService.deleteById(authenticationHelper.tryGetUser(session), id);
            return "redirect:/application/users";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return NOT_FOUND;
        } catch (UnauthorizedOperationException | AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return ACCESS_DENIED;
        }
    }


}
