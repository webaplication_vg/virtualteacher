package com.telerikacademy.web.virtualteacher.controllers.rest;

import com.telerikacademy.web.virtualteacher.config.AuthenticationHelper;
import com.telerikacademy.web.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.web.virtualteacher.exceptions.UnauthorizedOperationException;
import com.telerikacademy.web.virtualteacher.models.CoursesEntity;
import com.telerikacademy.web.virtualteacher.models.UsersCoursesEntity;
import com.telerikacademy.web.virtualteacher.models.UsersEntity;
import com.telerikacademy.web.virtualteacher.services.interfaces.CourseService;
import com.telerikacademy.web.virtualteacher.services.interfaces.UserCourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/courses")
public class CourseController {

    private final CourseService service;
    private final AuthenticationHelper authenticationHelper;
    private final UserCourseService userCourseService;

    @Autowired
    public CourseController(CourseService service, AuthenticationHelper authenticationHelper, UserCourseService userCourseService) {
        this.service = service;
        this.authenticationHelper = authenticationHelper;
        this.userCourseService = userCourseService;
    }

    @GetMapping
    public List<CoursesEntity> getAll() {
        return service.getAll();
    }

    @GetMapping("/search")
    public List<CoursesEntity> search(@RequestParam(required = false) Optional<String> word) {
        return service.searchAll(word);
    }

    @GetMapping("/{id}")
    public CoursesEntity getById(@PathVariable int id) {
        try {
            return service.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public CoursesEntity create(@RequestHeader HttpHeaders headers,
                                @Valid @RequestBody CoursesEntity course) {
        try {
            UsersEntity user = authenticationHelper.tryGetUser(headers);
            service.saveCourse(user, course);
            return course;
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public CoursesEntity update(@PathVariable int id,
                                @RequestHeader HttpHeaders headers,
                                @Valid @RequestBody CoursesEntity course) {
        try {
            UsersEntity user = authenticationHelper.tryGetUser(headers);
            course.setId(id);

            service.saveCourse(user, course);

            return course;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id, @RequestHeader HttpHeaders headers) {
        try {
            service.deleteById(authenticationHelper.tryGetUser(headers), id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping("/my-courses/{id}")
    public void addMyCourses(@PathVariable int id,
                             @RequestHeader HttpHeaders headers) {
        UsersCoursesEntity usersCoursesEntity = new UsersCoursesEntity();
        usersCoursesEntity.setCourse(service.getById(id));
        usersCoursesEntity.setUser(authenticationHelper.tryGetUser(headers));
        userCourseService.saveUserCourse(usersCoursesEntity);
    }
}
