package com.telerikacademy.web.virtualteacher.controllers.mvc;

import com.telerikacademy.web.virtualteacher.config.AuthenticationHelper;
import com.telerikacademy.web.virtualteacher.services.interfaces.CourseService;
import com.telerikacademy.web.virtualteacher.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.stream.Collectors;

@Controller
@RequestMapping("/")
public class HomeMvcController extends BaseClass {
    private final CourseService courseService;

    @Autowired
    public HomeMvcController(CourseService courseService, AuthenticationHelper authenticationHelper, UserService userService) {
        super(authenticationHelper, userService);
        this.courseService = courseService;
    }

    @GetMapping
    public String showHomePage(Model model) {
        model.addAttribute("topCourses", courseService.getAll().stream().limit(3).collect(Collectors.toList()));
        return "index";
    }

    @GetMapping("lucky-spin/")
    public String showLuckySpin() {
        return "luck";
    }


}
