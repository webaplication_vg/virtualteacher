package com.telerikacademy.web.virtualteacher.services.interfaces;

import com.telerikacademy.web.virtualteacher.models.StatusesEntity;
import com.telerikacademy.web.virtualteacher.models.TopicsEntity;

import java.util.List;

public interface StatusService {

	List<StatusesEntity> getAll();

	StatusesEntity getById(int id);



}
