package com.telerikacademy.web.virtualteacher.services.mappers;

import com.telerikacademy.web.virtualteacher.models.UsersEntity;
import com.telerikacademy.web.virtualteacher.models.dtos.RegisterDto;
import com.telerikacademy.web.virtualteacher.repositories.RoleRepository;
import com.telerikacademy.web.virtualteacher.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {
    private static final String DEFAULT_PICTURE = "default.png";
    private final RoleRepository roleRepository;

    @Autowired
    public UserMapper(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    public UsersEntity fromDto(RegisterDto dto){
        UsersEntity user = new UsersEntity();

        user.setFirstName(dto.getFirstName());
        user.setLastName(dto.getLastName());
        user.setEmail(dto.getEmail());
        user.setPassword(dto.getPassword());
        user.setRole(roleRepository.getById(1));
        user.setProfilePicture(DEFAULT_PICTURE);
        return user;
    }

}
