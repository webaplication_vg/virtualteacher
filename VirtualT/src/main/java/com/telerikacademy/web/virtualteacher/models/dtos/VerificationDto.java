package com.telerikacademy.web.virtualteacher.models.dtos;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

public class VerificationDto {

    private Integer code;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }
}
