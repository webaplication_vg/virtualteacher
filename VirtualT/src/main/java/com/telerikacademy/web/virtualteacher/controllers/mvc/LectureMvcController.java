package com.telerikacademy.web.virtualteacher.controllers.mvc;

import com.telerikacademy.web.virtualteacher.config.AuthenticationHelper;
import com.telerikacademy.web.virtualteacher.exceptions.AuthenticationFailureException;
import com.telerikacademy.web.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.web.virtualteacher.exceptions.UnauthorizedOperationException;
import com.telerikacademy.web.virtualteacher.models.AssignmentsEntity;
import com.telerikacademy.web.virtualteacher.models.CoursesEntity;
import com.telerikacademy.web.virtualteacher.models.LecturesEntity;
import com.telerikacademy.web.virtualteacher.models.UsersEntity;
import com.telerikacademy.web.virtualteacher.models.dtos.AssignmentDto;
import com.telerikacademy.web.virtualteacher.services.interfaces.*;
import com.telerikacademy.web.virtualteacher.services.mappers.AssignmentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping("/lectures")
public class LectureMvcController extends BaseClass {

    private final LectureService lectureService;
    private final CourseService courseService;
    private final AuthenticationHelper authenticationHelper;
    private final AssignmentMapper assignmentMapper;
    private final AssignmentService assignmentService;
    private final UserCourseService userCourseService;


    @Autowired
    public LectureMvcController(LectureService lectureService, CourseService courseService, AuthenticationHelper authenticationHelper, AssignmentMapper assignmentMapper, AssignmentService assignmentService, UserService userService, UserCourseService userCourseService) {
        super(authenticationHelper, userService);
        this.lectureService = lectureService;
        this.courseService = courseService;
        this.authenticationHelper = authenticationHelper;
        this.assignmentMapper = assignmentMapper;
        this.assignmentService = assignmentService;
        this.userCourseService = userCourseService;
    }

    @ModelAttribute("courses")
    public List<CoursesEntity> populateCourses() {
        return courseService.getAll();
    }

    @GetMapping
    public String showAllLectures(HttpSession session) {
        String x = checkIfLogged(session);
        if (x != null) return x;

        return LECTURES;
    }

    @GetMapping("/{id}")
    public String showLecturePage(@PathVariable int id, Model model, HttpSession session) {

        String x = checkIfLogged(session);
        if (x != null) return x;
        try {
            UsersEntity user = authenticationHelper.tryGetUser(session);
            model.addAttribute("lecture", lectureService.getById(id));
            model.addAttribute("submitted", assignmentService.getByUserIdAndLectureId(user.getId(), id));
            model.addAttribute("userCourse", userCourseService.getByCourseId(lectureService.getById(id).getCourse().getId(), user));
            model.addAttribute("assignment", new AssignmentDto());

            return "lecture";
        } catch (UnauthorizedOperationException | AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return ACCESS_DENIED;
        }
    }

    @GetMapping("/new/{courseId}")
    public String showNewLecturePage(@PathVariable int courseId,
                                     Model model,
                                     HttpSession session) {

        try {
            authenticationHelper.tryGetUser(session);
            model.addAttribute("course", courseService.getCourse(courseId));
            model.addAttribute("lecture", new LecturesEntity());
            return LECTURE_NEW;
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return ACCESS_DENIED;
        }

    }

    @PostMapping("/new/{courseId}")
    public String createLecturePage(@PathVariable int courseId,
                                    @Valid LecturesEntity lecture,
                                    BindingResult errors,
                                    Model model,
                                    HttpSession session) {
        if (errors.hasErrors()) {
            return LECTURE_NEW;
        }
        try {
            UsersEntity user = authenticationHelper.tryGetUser(session);
            lecture.setCourse(courseService.getById(courseId));
            lectureService.saveLecture(user, lecture);
            return "redirect:/courses/" + courseId;
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return ACCESS_DENIED;
        }
    }

    @GetMapping("/update/{lectureId}")
    public String showEditLecturePage(@PathVariable int lectureId,
                                      Model model,
                                      HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return ACCESS_DENIED;
        }
        model.addAttribute("course", courseService.getCourse(lectureService.getById(lectureId).getCourse().getId()));
        model.addAttribute("lecture", lectureService.getById(lectureId));
        return LECTURE_EDIT;

    }

    @PostMapping("/update/{lectureId}")
    public String editLecture(@PathVariable int lectureId,
                              @Valid @ModelAttribute("lecture") LecturesEntity lecture,
                              BindingResult errors,
                              Model model,
                              HttpSession session) {
        if (errors.hasErrors()) {
            return LECTURE_EDIT;
        }
        try {
            UsersEntity user = authenticationHelper.tryGetUser(session);
            lecture.setId(lectureId);
            lectureService.saveLecture(user, lecture);
            return "redirect:/courses/" + lecture.getCourse().getId();
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return NOT_FOUND;
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return ACCESS_DENIED;
        }
    }

    @GetMapping("/delete/{lectureId}")
    public String deleteLecture(@PathVariable int lectureId, Model model, HttpSession session) {
        try {
            int courseId = lectureService.getById(lectureId).getCourse().getId();
            lectureService.deleteById(authenticationHelper.tryGetUser(session), lectureId);
            return "redirect:/courses/" + courseId;
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return NOT_FOUND;
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return ACCESS_DENIED;
        }
    }

    @PostMapping("/upload/{lectureId}")
    public String uploadAssignment(@PathVariable int lectureId,
                                   @Valid @ModelAttribute("assignment") AssignmentDto assignmentDto,
                                   BindingResult errors,
                                   Model model,
                                   HttpSession session) throws IOException {
        if (errors.hasErrors()) {
            return LECTURES;
        }
        try {
            UsersEntity user = authenticationHelper.tryGetUser(session);
            AssignmentsEntity assignmentsEntity = assignmentMapper.fromDto(assignmentDto);
            assignmentsEntity.setUser(user);
            assignmentsEntity.setLecture(lectureService.getById(lectureId));
            assignmentService.saveAssignment(user, assignmentsEntity);
            return "redirect:/lectures/" + lectureId;
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return NOT_FOUND;
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return ACCESS_DENIED;
        }
    }
}
