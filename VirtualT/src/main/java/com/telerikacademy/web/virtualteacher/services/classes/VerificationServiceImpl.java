package com.telerikacademy.web.virtualteacher.services.classes;



import com.telerikacademy.web.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.web.virtualteacher.models.VerificationsEntity;
import com.telerikacademy.web.virtualteacher.repositories.VerificationRepository;
import com.telerikacademy.web.virtualteacher.services.interfaces.VerificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VerificationServiceImpl implements VerificationService {

    private final VerificationRepository repository;

    @Autowired
    public VerificationServiceImpl(VerificationRepository repository) {
        this.repository = repository;
    }


    @Override
    public VerificationsEntity getByData(String username, String password, int code) {
        List<VerificationsEntity> verifications = repository.getByData(username,password,code);
       if(!verifications.isEmpty()){
           return verifications.get(0);
       }
        throw new EntityNotFoundException("Verification not valid!");
    }

    @Override
    public void create(VerificationsEntity verificationsEnt) {
        repository.save(verificationsEnt);
    }
}
