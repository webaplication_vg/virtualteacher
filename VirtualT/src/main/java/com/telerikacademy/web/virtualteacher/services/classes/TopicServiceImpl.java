package com.telerikacademy.web.virtualteacher.services.classes;

import com.telerikacademy.web.virtualteacher.exceptions.DuplicateEntityException;
import com.telerikacademy.web.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.web.virtualteacher.exceptions.UnauthorizedOperationException;
import com.telerikacademy.web.virtualteacher.models.CoursesEntity;
import com.telerikacademy.web.virtualteacher.models.TopicsEntity;
import com.telerikacademy.web.virtualteacher.models.UsersEntity;
import com.telerikacademy.web.virtualteacher.repositories.CourseRepository;
import com.telerikacademy.web.virtualteacher.repositories.TopicRepository;
import com.telerikacademy.web.virtualteacher.services.interfaces.CourseService;
import com.telerikacademy.web.virtualteacher.services.interfaces.TopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TopicServiceImpl implements TopicService {

    private final TopicRepository repository;

    @Autowired
    public TopicServiceImpl(TopicRepository repository) {
        this.repository = repository;
    }


    @Override
    public List<TopicsEntity> getAll() {
        return repository.findAll();
    }

    @Override
    public TopicsEntity getById(int id) {
        Optional<TopicsEntity> optional = repository.findById(id);
        TopicsEntity topic = null;
        if (optional.isPresent()) {
            topic = optional.get();
        } else {
            throw new EntityNotFoundException("Topic", "id", String.valueOf(id));
        }
        return topic;
    }

    @Override
    public void saveTopic(TopicsEntity topic) {
        repository.save(topic);
    }
}
