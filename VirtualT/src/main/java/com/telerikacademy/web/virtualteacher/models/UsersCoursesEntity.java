package com.telerikacademy.web.virtualteacher.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "users_courses", schema = "virtualteacher", catalog = "")
public class UsersCoursesEntity {
    private int id;
    private int courseRating;
    private UsersEntity user;
    private CoursesEntity course;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    @Column(name = "course_rating")
    public int getCourseRating() {
        return courseRating;
    }

    public void setCourseRating(int courseRating) {
        this.courseRating = courseRating;
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    public UsersEntity getUser() {
        return user;
    }

    public void setUser(UsersEntity usersByUserId) {
        this.user = usersByUserId;
    }

    @ManyToOne
    @JoinColumn(name = "course_id", referencedColumnName = "id")
    public CoursesEntity getCourse() {
        return course;
    }

    public void setCourse(CoursesEntity coursesByCourseId) {
        this.course = coursesByCourseId;
    }
}
