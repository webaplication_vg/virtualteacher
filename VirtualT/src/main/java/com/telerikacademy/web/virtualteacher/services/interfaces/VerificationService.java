package com.telerikacademy.web.virtualteacher.services.interfaces;


import com.telerikacademy.web.virtualteacher.models.VerificationsEntity;

public interface VerificationService {
    VerificationsEntity getByData(String username, String password, int code);

    void create(VerificationsEntity verificationsEnt);
}
