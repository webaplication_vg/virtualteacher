package com.telerikacademy.web.virtualteacher.services.classes;

import com.telerikacademy.web.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.web.virtualteacher.models.RolesEntity;
import com.telerikacademy.web.virtualteacher.models.TopicsEntity;
import com.telerikacademy.web.virtualteacher.repositories.LectureRepository;
import com.telerikacademy.web.virtualteacher.repositories.RoleRepository;
import com.telerikacademy.web.virtualteacher.repositories.TopicRepository;
import com.telerikacademy.web.virtualteacher.services.interfaces.RoleService;
import com.telerikacademy.web.virtualteacher.services.interfaces.TopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RoleServiceImpl implements RoleService {

    private final RoleRepository repository;

    @Autowired
    public RoleServiceImpl(RoleRepository repository) {
        this.repository = repository;
    }


    @Override
    public List<RolesEntity> getAll() {
        return repository.findAll();
    }

    @Override
    public RolesEntity getById(int id) {
        Optional<RolesEntity> optional = repository.findById(id);
        RolesEntity role = null;
        if (optional.isPresent()) {
            role = optional.get();
        } else {
            throw new EntityNotFoundException("Role", "id", String.valueOf(id));
        }
        return role;
    }
}
