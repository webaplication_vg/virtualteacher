package com.telerikacademy.web.virtualteacher.models.dtos;

import com.telerikacademy.web.virtualteacher.models.LecturesEntity;
import com.telerikacademy.web.virtualteacher.models.UsersEntity;
import org.springframework.web.multipart.MultipartFile;

public class MultipartFileDto {

    private MultipartFile file;

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }
}
