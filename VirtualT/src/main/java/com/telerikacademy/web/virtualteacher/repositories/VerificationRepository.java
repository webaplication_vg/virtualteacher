package com.telerikacademy.web.virtualteacher.repositories;

import com.telerikacademy.web.virtualteacher.models.VerificationsEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VerificationRepository extends JpaRepository<VerificationsEntity, Integer> {

    @Query(value = "select * from virtualteacher.verifications join virtualteacher.users u on u.id = verifications.user_id where email = :email and password = :password and verification_code = :code", nativeQuery = true)
    List<VerificationsEntity> getByData(@Param("email") String email, @Param("password") String password, @Param("code") int code);


}