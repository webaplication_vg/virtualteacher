package com.telerikacademy.web.virtualteacher.controllers.mvc;

import com.telerikacademy.web.virtualteacher.config.AuthenticationHelper;
import com.telerikacademy.web.virtualteacher.exceptions.AuthenticationFailureException;
import com.telerikacademy.web.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.web.virtualteacher.exceptions.UnauthorizedOperationException;
import com.telerikacademy.web.virtualteacher.models.*;
import com.telerikacademy.web.virtualteacher.services.interfaces.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/courses")
public class CourseMvcController extends BaseClass {


    private final CourseService courseService;
    private final UserCourseService userCourseService;
    private final LectureService lectureService;
    private final TopicService topicService;
    private final StatusService statusService;
    private final AssignmentService assignmentService;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public CourseMvcController(CourseService courseService, UserCourseService userCourseService, LectureService lectureService, TopicService topicService, StatusService statusService, AssignmentService assignmentService, AuthenticationHelper authenticationHelper, UserService userService) {
        super(authenticationHelper, userService);
        this.courseService = courseService;
        this.userCourseService = userCourseService;
        this.lectureService = lectureService;
        this.topicService = topicService;
        this.statusService = statusService;
        this.assignmentService = assignmentService;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("topics")
    public List<TopicsEntity> populateTopics() {
        return topicService.getAll();
    }

    @ModelAttribute("statuses")
    public List<StatusesEntity> populateStatuses() {
        return statusService.getAll();
    }


    @GetMapping
    public String showAllCoursesPage(@RequestParam(defaultValue = "") Optional<String> search,
                                     Model model, HttpSession session) {
        try {
            model.addAttribute("courses", courseService.searchAll(search));
            model.addAttribute("myCourses", courseService.getMyCourses(authenticationHelper.tryGetUser(session).getId()));
        } catch (UnauthorizedOperationException | AuthenticationFailureException e) {
            return ACCESS_DENIED;
        }
        return "courses";
    }

    @GetMapping("/{id}")
    public String showCoursePage(@PathVariable int id,
                                 @RequestParam(defaultValue = "") Optional<String> search,
                                 Model model,
                                 HttpSession session) {

        try {
            UsersEntity user = authenticationHelper.tryGetUser(session);
            model.addAttribute("course", courseService.getCourse(id));
            model.addAttribute("lectures", lectureService.searchAll(search, id));
            model.addAttribute("userCourse", userCourseService.getByCourseId(id, user));
            model.addAttribute("averageGrade", assignmentService.averageGrade(user.getId(), id));

            return "lectures";
        } catch (UnauthorizedOperationException | AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return ACCESS_DENIED;
        }
    }

    @GetMapping("/new")
    public String showNewCoursePage(Model model, HttpSession session) {

        try {
            authenticationHelper.tryGetUser(session);
            model.addAttribute("topic", new TopicsEntity());
            model.addAttribute("course", new CoursesEntity());
            return COURSE_NEW;
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return NOT_FOUND;
        } catch (UnauthorizedOperationException | AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return ACCESS_DENIED;
        }
    }

    @PostMapping("/new")
    public String createCoursePage(@Valid CoursesEntity course,
                                   BindingResult errors,
                                   Model model,
                                   HttpSession session) {
        if (errors.hasErrors()) {
            return COURSE_NEW;
        }
        try {
            UsersEntity user = authenticationHelper.tryGetUser(session);
            courseService.saveCourse(user, course);
            return "redirect:/courses";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return NOT_FOUND;
        } catch (UnauthorizedOperationException | AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return ACCESS_DENIED;
        }
    }

    @GetMapping("/update/{id}")
    public String showEditCoursePage(@PathVariable int id,
                                     Model model,
                                     HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
            model.addAttribute("topic", new TopicsEntity());
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return ACCESS_DENIED;
        }
        model.addAttribute("updatedCourse", courseService.getById(id));
        model.addAttribute("course", new CoursesEntity());
        return COURSE_EDIT;
    }

    @PostMapping("/update/{id}")
    public String editCourse(@PathVariable int id,
                             @Valid @ModelAttribute("course") CoursesEntity course,
                             BindingResult errors,
                             Model model,
                             HttpSession session) {
        if (errors.hasErrors()) {
            return COURSE_EDIT;
        }
        try {
            UsersEntity user = authenticationHelper.tryGetUser(session);
            course.setId(id);
            courseService.saveCourse(user, course);
            return "redirect:/courses";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return NOT_FOUND;
        } catch (UnauthorizedOperationException | AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return ACCESS_DENIED;
        }
    }

    @GetMapping("/delete/{id}")
    public String deleteCourse(@PathVariable int id, Model model, HttpSession session) {
        try {
            courseService.deleteById(authenticationHelper.tryGetUser(session), id);
            return "redirect:/courses";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return NOT_FOUND;
        } catch (UnauthorizedOperationException | AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return ACCESS_DENIED;
        }
    }

    @GetMapping("/my-courses/{id}")
    public String addMyCourses(@PathVariable int id, Model model, HttpSession session) {
        try {
            UsersCoursesEntity usersCoursesEntity = new UsersCoursesEntity();
            usersCoursesEntity.setCourse(courseService.getById(id));
            usersCoursesEntity.setUser(authenticationHelper.tryGetUser(session));
            userCourseService.saveUserCourse(usersCoursesEntity);
            return "redirect:/courses";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return NOT_FOUND;
        } catch (UnauthorizedOperationException | AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return ACCESS_DENIED;
        }
    }

    @PostMapping("/new-topic")
    public String createTopic(@Valid TopicsEntity topicsEntity,
                              Model model) {
        try {
            topicService.saveTopic(topicsEntity);
            return "redirect:/courses";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return NOT_FOUND;
        } catch (UnauthorizedOperationException | AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return ACCESS_DENIED;
        }
    }

    @PostMapping("/rate/{id}")
    public String rateCourse(@PathVariable int id,
                             @Valid UsersCoursesEntity userCourse,
                             BindingResult errors,
                             Model model,
                             HttpSession session) {
        if (errors.hasErrors()) {
            return "my-courses";
        }
        try {
            UsersEntity user = authenticationHelper.tryGetUser(session);
            userCourse.setId(id);
            userCourseService.saveUserCourse(userCourse);
            courseService.setRating(userCourse, user);
            return "redirect:/courses/" + userCourse.getCourse().getId();
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return NOT_FOUND;
        } catch (UnauthorizedOperationException | AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return ACCESS_DENIED;
        }
    }
}
