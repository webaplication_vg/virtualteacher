package com.telerikacademy.web.virtualteacher.services.interfaces;

import com.telerikacademy.web.virtualteacher.models.UsersCoursesEntity;
import com.telerikacademy.web.virtualteacher.models.UsersEntity;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface UserCourseService {

    List<UsersCoursesEntity> getAll();

    UsersCoursesEntity getById(int id);

    UsersCoursesEntity getByCourseId(int courseId, UsersEntity user);

    void saveUserCourse(UsersCoursesEntity usersCoursesEntity);

    List<UsersCoursesEntity> searchAll(Optional<String> search, int id);

    Double getCourseSumRating(int courseId);

    Double getUserCountByCourseId(int courseId);

}
