package com.telerikacademy.web.virtualteacher.controllers.mvc;

import com.telerikacademy.web.virtualteacher.config.AuthenticationHelper;
import com.telerikacademy.web.virtualteacher.exceptions.AuthenticationFailureException;
import com.telerikacademy.web.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.web.virtualteacher.exceptions.UnauthorizedOperationException;
import com.telerikacademy.web.virtualteacher.models.AssignmentsEntity;
import com.telerikacademy.web.virtualteacher.models.GradesEntity;
import com.telerikacademy.web.virtualteacher.models.UsersEntity;
import com.telerikacademy.web.virtualteacher.services.interfaces.AssignmentService;
import com.telerikacademy.web.virtualteacher.services.interfaces.FileService;
import com.telerikacademy.web.virtualteacher.services.interfaces.GradesService;
import com.telerikacademy.web.virtualteacher.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/assignments")
public class AssignmentMvcController extends BaseClass {


    private final AssignmentService assignmentService;
    private final FileService fileService;
    private final GradesService gradesService;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public AssignmentMvcController(AssignmentService assignmentService, FileService fileService, GradesService gradesService, AuthenticationHelper authenticationHelper, UserService userService) {
        super(authenticationHelper, userService);
        this.assignmentService = assignmentService;
        this.fileService = fileService;
        this.gradesService = gradesService;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("grades")
    public List<GradesEntity> populateGrades() {
        return gradesService.getAll().stream().sorted(Comparator.comparing(GradesEntity::getId).reversed()).collect(Collectors.toList());
    }

    @GetMapping
    public String showAllAssignments(HttpSession session) {
        String x = checkIfLogged(session);
        if (x != null) return x;

        return "redirect:/assignments/page/1/id/desc";
    }

    @GetMapping("/user/{id}")
    public String showUserAssignments(@PathVariable int id, Model model, HttpSession session) {
        String x = checkIfLogged(session);
        if (x != null) return x;

        model.addAttribute("assignments", assignmentService.getAllByUserId(id));
        return ASSIGNMENTS_TABLE;
    }

    @GetMapping("/{id}")
    public String showAssignmentGrade(@PathVariable int id, Model model, HttpSession session) throws IOException {
        String x = checkIfLogged(session);
        if (x != null) return x;

        model.addAttribute("assignment", assignmentService.getById(id));
        model.addAttribute("path", fileService.getFile(assignmentService.getById(id).getFile()));
        return ASSIGNMENT_GRADE;
    }

    @GetMapping("/update/{id}")
    public String showEditAssignmentPage(@PathVariable int id,
                                         Model model,
                                         HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
            model.addAttribute("assignment", new AssignmentsEntity());
        } catch (UnauthorizedOperationException | AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return ACCESS_DENIED;
        }
        model.addAttribute("updatedAssignment", assignmentService.getById(id));
        model.addAttribute("assignment", new AssignmentsEntity());
        return ASSIGNMENT_GRADE;
    }

    @PostMapping("/update/{id}")
    public String editAssignment(@PathVariable int id,
                                 @Valid @ModelAttribute("assignment") AssignmentsEntity assignment,
                                 BindingResult errors,
                                 Model model,
                                 HttpSession session) {

        if (errors.hasErrors()) {
            return ASSIGNMENT_GRADE;
        }
        try {
            UsersEntity user = authenticationHelper.tryGetUser(session);
            assignment.setId(id);
            assignmentService.saveAssignment(user, assignment);
            return "redirect:/assignments";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return NOT_FOUND;
        } catch (UnauthorizedOperationException | AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return ACCESS_DENIED;
        }
    }

    @GetMapping("/page/{pageNo}/{sortField}/{sortDir}")
    public String findPaginated(@PathVariable(value = "pageNo") int pageNo,
                                @PathVariable(value = "sortField") String sortField,
                                @PathVariable(value = "sortDir") String sortDir,
                                @RequestParam(value = "search", defaultValue = "") String search,
                                Model model) {
        int pageSize = 10;

        Page<AssignmentsEntity> page = assignmentService.findPaginated(pageNo, pageSize, sortField, sortDir, search);
        List<AssignmentsEntity> listAssignments = page.getContent();

        model.addAttribute("currentPage", pageNo);
        model.addAttribute("totalPages", page.getTotalPages());
        model.addAttribute("totalItems", page.getTotalElements());
        model.addAttribute("search", search);
        model.addAttribute("sortField", sortField);
        model.addAttribute("sortDir", sortDir);
        model.addAttribute("reverseSortDir", sortDir.equals("asc") ? "desc" : "asc");
        model.addAttribute("listAssignments", listAssignments);
        return ASSIGNMENTS_TABLE;
    }

}
