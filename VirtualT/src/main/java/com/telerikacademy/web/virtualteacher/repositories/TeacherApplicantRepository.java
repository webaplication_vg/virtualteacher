package com.telerikacademy.web.virtualteacher.repositories;

import com.telerikacademy.web.virtualteacher.models.TeacherApplicantsEntity;
import com.telerikacademy.web.virtualteacher.models.UsersEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface TeacherApplicantRepository extends JpaRepository<TeacherApplicantsEntity, Integer> {

    @Query(value = "select * from virtualteacher.teacher_applicants join virtualteacher.users u on u.id = teacher_applicants.user_id where (first_name like concat('%',:search,'%') or last_name like concat('%',:search,'%') or email like concat('%',:search,'%'))", nativeQuery = true)
    List<TeacherApplicantsEntity> searchAll(@Param("search") Optional<String> search);

    @Query(value = "select * from virtualteacher.teacher_applicants where :userId=user_id", nativeQuery = true)
    List<TeacherApplicantsEntity> getByUserId(@Param("userId") int userId);

}