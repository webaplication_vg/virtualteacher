package com.telerikacademy.web.virtualteacher.services.interfaces;

import com.telerikacademy.web.virtualteacher.models.AssignmentsEntity;
import com.telerikacademy.web.virtualteacher.models.GradesEntity;

import java.util.List;

public interface GradesService {

	List<GradesEntity> getAll();

	GradesEntity getById(int id);
}
