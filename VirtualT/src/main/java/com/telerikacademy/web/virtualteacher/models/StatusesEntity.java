package com.telerikacademy.web.virtualteacher.models;

import javax.persistence.*;

@Entity
@Table(name = "statuses", schema = "virtualteacher", catalog = "")
public class StatusesEntity {
    private int id;
    private String name;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 50)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
