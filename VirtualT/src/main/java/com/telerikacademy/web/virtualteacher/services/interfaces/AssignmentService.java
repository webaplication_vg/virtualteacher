package com.telerikacademy.web.virtualteacher.services.interfaces;

import com.telerikacademy.web.virtualteacher.models.AssignmentsEntity;
import com.telerikacademy.web.virtualteacher.models.UsersEntity;
import org.springframework.data.domain.Page;

import java.util.List;

public interface AssignmentService {

    List<AssignmentsEntity> getAll();

    List<AssignmentsEntity> getAllByUserId(int id);

    List<AssignmentsEntity> getByUserIdAndLectureId(int userId,int lectureId);

    AssignmentsEntity getById(int id);

    double averageGrade(int userId,int courseId);

    void saveAssignment(UsersEntity initiator, AssignmentsEntity assignmentsEntity);

    Page<AssignmentsEntity> findPaginated(int pageNo, int pageSize, String sortField, String sortDirection,String search);

}
