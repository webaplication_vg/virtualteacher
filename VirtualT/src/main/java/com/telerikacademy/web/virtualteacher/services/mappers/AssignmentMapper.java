package com.telerikacademy.web.virtualteacher.services.mappers;

import com.telerikacademy.web.virtualteacher.models.AssignmentsEntity;
import com.telerikacademy.web.virtualteacher.models.dtos.AssignmentDto;
import com.telerikacademy.web.virtualteacher.repositories.GradeRepository;
import com.telerikacademy.web.virtualteacher.repositories.RoleRepository;
import com.telerikacademy.web.virtualteacher.repositories.UserRepository;
import com.telerikacademy.web.virtualteacher.services.interfaces.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class AssignmentMapper {
    public static final int DEFAULT_GRADE = 1;
    private final FileService fileService;
    private final GradeRepository gradeRepository;

    @Autowired
    public AssignmentMapper(FileService fileService, GradeRepository gradeRepository) {
        this.fileService = fileService;
        this.gradeRepository = gradeRepository;
    }


    public AssignmentsEntity fromDto(AssignmentDto assignmentDto) throws IOException {
        AssignmentsEntity assignmentsEntity = new AssignmentsEntity();
        fileService.uploadFile(assignmentDto.getFile());
        assignmentsEntity.setFile(assignmentDto.getFile().getOriginalFilename());
        assignmentsEntity.setUser(assignmentDto.getUser());
        assignmentsEntity.setLecture(assignmentDto.getLecture());
        assignmentsEntity.setGrade(gradeRepository.getById(DEFAULT_GRADE));

        return assignmentsEntity;
    }

}
