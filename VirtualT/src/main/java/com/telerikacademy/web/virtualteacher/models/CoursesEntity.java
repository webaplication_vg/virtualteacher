package com.telerikacademy.web.virtualteacher.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "courses", schema = "virtualteacher", catalog = "")
public class CoursesEntity {
    private int id;
    private String name;
    private String picture;
    private String description;
    private Date startingDate;
    private TopicsEntity topic;
    private StatusesEntity status;
    private double rating;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = true, length = 30)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @JsonIgnore
    @Column(name = "picture", nullable = true, length = 500)
    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    @Column(name = "rating")
    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    @Basic
    @Column(name = "description", nullable = true, length = 1000)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "starting_date", nullable = true)
    public Date getStartingDate() {
        return startingDate;
    }

    public void setStartingDate(Date startingDate) {
        this.startingDate = startingDate;
    }


    @ManyToOne
    @JoinColumn(name = "topic_id", referencedColumnName = "id")
    public TopicsEntity getTopic() {
        return topic;
    }

    public void setTopic(TopicsEntity topicsByTopicId) {
        this.topic = topicsByTopicId;
    }

    @ManyToOne
    @JoinColumn(name = "status_id", referencedColumnName = "id")
    public StatusesEntity getStatus() {
        return status;
    }

    public void setStatus(StatusesEntity statusByStatusId) {
        this.status = statusByStatusId;
    }

}
