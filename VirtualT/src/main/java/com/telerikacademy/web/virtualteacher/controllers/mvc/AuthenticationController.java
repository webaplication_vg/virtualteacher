package com.telerikacademy.web.virtualteacher.controllers.mvc;

import com.telerikacademy.web.virtualteacher.config.AuthenticationHelper;
import com.telerikacademy.web.virtualteacher.exceptions.AuthenticationFailureException;
import com.telerikacademy.web.virtualteacher.exceptions.DuplicateEntityException;
import com.telerikacademy.web.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.web.virtualteacher.models.UsersEntity;
import com.telerikacademy.web.virtualteacher.repositories.RoleRepository;
import com.telerikacademy.web.virtualteacher.models.dtos.LoginDto;
import com.telerikacademy.web.virtualteacher.models.dtos.RegisterDto;
import com.telerikacademy.web.virtualteacher.models.dtos.VerificationDto;
import com.telerikacademy.web.virtualteacher.services.interfaces.UserService;
import com.telerikacademy.web.virtualteacher.services.mappers.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.server.ResponseStatusException;


import javax.mail.MessagingException;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.Objects;


@Controller
@RequestMapping
public class AuthenticationController extends BaseClass {

    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;
    private final UserMapper userMapper;
    private final RoleRepository roleRepository;

    @Autowired
    public AuthenticationController(UserService userService, AuthenticationHelper authenticationHelper, UserMapper userMapper, RoleRepository roleRepository) {
        super(authenticationHelper, userService);
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
        this.userMapper = userMapper;
        this.roleRepository = roleRepository;
    }

    @GetMapping("/login")
    public String showLoginPage(Model model) {
        model.addAttribute(LOGIN, new LoginDto());
        return LOGIN;
    }

    @PostMapping("/login")
    public String handleLogin(@Valid @ModelAttribute(LOGIN) LoginDto login,
                              BindingResult bindingResult,
                              HttpSession session) {
        if (bindingResult.hasErrors()) {
            return LOGIN;
        }
        try {
            authenticationHelper.verifyAuthentication(login.getEmail(), login.getPassword());
            session.setAttribute("currentUser", login.getEmail());
            return "redirect:/";
        } catch (AuthenticationFailureException e) {
            bindingResult.rejectValue("email", "auth_error", e.getMessage());
            return LOGIN;
        }
    }

    @GetMapping("/logout")
    public String handleLogout(HttpSession session) {
        session.removeAttribute("currentUser");
        return "redirect:/";
    }

    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute(REGISTER, new RegisterDto());
        return REGISTER;
    }

    @PostMapping("/register")
    public String handleRegister(@Valid @ModelAttribute(REGISTER) RegisterDto register,
                                 BindingResult bindingResult, HttpSession session) {
        if (bindingResult.hasErrors()) {
            return REGISTER;
        }
        if (!register.getPassword().equals(register.getPasswordConfirm())) {
            bindingResult.rejectValue("passwordConfirm", "password_error", "Password confirmation should match password.");
            return REGISTER;
        }
        try {
            UsersEntity user = userMapper.fromDto(register);
            userService.saveUser(user);
            session.setAttribute("currentUser", register.getEmail());
            return "redirect:/";
        } catch (DuplicateEntityException | MessagingException e) {
            bindingResult.rejectValue("email", "email_error", e.getMessage());
            return REGISTER;
        }
    }

    @GetMapping("/verification")
    public String showVerificationPage(Model model) {
        model.addAttribute(VERIFICATION, new VerificationDto());
        return VERIFICATION;
    }

    @PostMapping("/verification")
    public String handleVerification(@Valid @ModelAttribute(VERIFICATION) VerificationDto verificationDto,
                                     BindingResult bindingResult, HttpSession session) {
        if (bindingResult.hasErrors()) {
            return VERIFICATION;
        }
        try {
            UsersEntity userToUpdate = authenticationHelper.tryGetUser(session);
            authenticationHelper.verifyAuthentication(userToUpdate.getEmail(), userToUpdate.getPassword(), verificationDto.getCode());
            userToUpdate.setRole(roleRepository.getById(2));

            userService.updateUser(userToUpdate, userToUpdate);
            return "redirect:/";
        } catch (AuthenticationFailureException | DuplicateEntityException e) {
            bindingResult.rejectValue("code", "error", e.getMessage());
            return VERIFICATION;
        } catch (ResponseStatusException e) {
            bindingResult.rejectValue("code", "error", Objects.requireNonNull(e.getMessage()));
            return VERIFICATION;
        } catch (EntityNotFoundException e) {
            bindingResult.rejectValue("code", "error", Objects.requireNonNull(e.getMessage()));
            return VERIFICATION;
        }
    }
}
