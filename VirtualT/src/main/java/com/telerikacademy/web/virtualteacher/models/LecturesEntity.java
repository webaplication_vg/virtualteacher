package com.telerikacademy.web.virtualteacher.models;

import javax.persistence.*;

@Entity
@Table(name = "lectures", schema = "virtualteacher", catalog = "")
public class LecturesEntity {
    private int id;
    private String name;
    private String description;
    private String assignment;
    private String videoUrl;
    private CoursesEntity course;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = true, length = 50)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "description", nullable = true, length = 1000)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "assignment", nullable = true, length = 500)
    public String getAssignment() {
        return assignment;
    }

    public void setAssignment(String assignment) {
        this.assignment = assignment;
    }

    @Basic
    @Column(name = "video_url", nullable = true, length = 500)
    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }


    @ManyToOne
    @JoinColumn(name = "course_id", referencedColumnName = "id")
    public CoursesEntity getCourse() {
        return course;
    }

    public void setCourse(CoursesEntity coursesByCourseId) {
        this.course = coursesByCourseId;
    }
}
