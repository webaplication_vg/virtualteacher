function runSpeechRecognition() {
    // get output div reference
    var output = document.getElementById("output");
    // get action element reference
    var action = document.getElementById("action");
    // new speech recognition object
    var SpeechRecognition = SpeechRecognition || webkitSpeechRecognition;
    var recognition = new SpeechRecognition();
    recognition.lang = 'en-US';


    recognition.onspeechend = function () {
        recognition.stop();
    }

    // This runs when the speech recognition service returns result
    recognition.onresult = function (event) {
        var transcript = event.results[0][0].transcript;
        var search;
        if (transcript.includes("how are you")) {
            window.open("https://www.youtube.com/watch?v=Bu8bH2P37kY&ab_channel=TacheMemes")
        }

        if (transcript.includes("open YouTube")) {
            search = transcript.replace("open YouTube ","").replace("open YouTube","")
            window.open("https://www.youtube.com/results?search_query=" + search)
        }

        if (transcript.includes("courses")) {
            search=transcript.replace("show me ","").replace("courses","");
            window.open("http://localhost:8080/courses?search=" + search)
        }

        if (transcript.includes("show project info")) {
            window.open("https://gitlab.com/webaplication_vg/virtualteacher/-/blob/main/README.md")
        }

        if (transcript.includes("play song")) {
            window.open("https://www.youtube.com/watch?v=fJ9rUzIMcZQ&ab_channel=QueenOfficial")
        }

        if (transcript.includes("teacher applicant")) {
            window.open("http://localhost:8080/application/users")
        }

        if (transcript.includes("lucky")) {
            window.open("http://localhost:8080/lucky-spin/")
        }

        if (transcript.includes("polymorphism")) {
            window.open("https://www.geeksforgeeks.org/polymorphism-in-java/")
        }

        if (transcript.includes("encapsulation")) {
            window.open("https://www.geeksforgeeks.org/encapsulation-in-java/")
        }

        if (transcript.includes("inheritance")) {
            window.open("https://www.geeksforgeeks.org/inheritance-in-java/")
        }

        if (transcript.includes("abstraction")) {
            window.open("https://www.geeksforgeeks.org/abstraction-in-java-2/")
        }

        if (transcript.includes("questions")) {
            window.open("https://www.edureka.co/blog/interview-questions/java-interview-questions/")
        }

         if (transcript.includes("exception")) {
            window.open("https://www.geeksforgeeks.org/exceptions-in-java/")
        }

        output.innerHTML = "Searching for: " + transcript;
        output.classList.remove("hide");

    };



    // start recognition
    recognition.start();


}