package com.telerikacademy.web.virtualteacher.services;

import com.telerikacademy.web.virtualteacher.Helpers;
import com.telerikacademy.web.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.web.virtualteacher.repositories.GradeRepository;
import com.telerikacademy.web.virtualteacher.services.classes.GradesServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class GradesServiceTests {

    @Mock
    GradeRepository mockRepository;
    @InjectMocks
    GradesServiceImpl service;

    @Test
    public void getAll_should_callRepository() {
        Mockito.when(mockRepository.findAll())
                .thenReturn(new ArrayList<>());

        service.getAll();

        Mockito.verify(mockRepository, Mockito.times(1)).findAll();
    }

    @Test
    public void getById_should_returnRole_when_matchExists() {
        var grade = Helpers.createMockGrade();

        Mockito.when(mockRepository.findById(grade.getId()))
                .thenReturn(Optional.of(grade));

        var result = service.getById(grade.getId());

        Assertions.assertEquals(grade.getName(), result.getName());
    }

    @Test
    public void getById_should_throwException_when_matchDoesntExist() {
        int nonExistingGradeId = 20;

        Assertions.assertThrows(EntityNotFoundException.class, () -> service.getById(nonExistingGradeId));

    }

}
