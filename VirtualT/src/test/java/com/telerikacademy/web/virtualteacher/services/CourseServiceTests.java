package com.telerikacademy.web.virtualteacher.services;

import com.telerikacademy.web.virtualteacher.Helpers;
import com.telerikacademy.web.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.web.virtualteacher.exceptions.UnauthorizedOperationException;
import com.telerikacademy.web.virtualteacher.repositories.CourseRepository;
import com.telerikacademy.web.virtualteacher.services.classes.CourseServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class CourseServiceTests {
    @Mock
    CourseRepository mockRepository;
    @InjectMocks
    CourseServiceImpl service;

    @Test
    public void getAll_should_callRepository() {
        Mockito.when(mockRepository.findAll())
                .thenReturn(new ArrayList<>());

        service.getAll();

        Mockito.verify(mockRepository, Mockito.times(1)).findAll();
    }

    @Test
    public void getById_should_returnCourse_when_matchExists() {
        var course = Helpers.createMockCourse();

        Mockito.when(mockRepository.findById(course.getId()))
                .thenReturn(Optional.of(course));

        var result = service.getById(course.getId());

        Assertions.assertEquals(course.getName(), result.getName());
    }


    @Test
    public void getById_should_throwException_when_matchDoesntExist() {
        int nonExistingId = 200;

        Assertions.assertThrows(EntityNotFoundException.class, () -> service.getById(nonExistingId));

    }

    @Test
    public void delete_should_callRepository() {
        var user = Helpers.createMockUser();
        var role = Helpers.createMockRole();
        role.setId(4);
        user.setRole(role);
        var course = Helpers.createMockCourse();

        service.deleteById(user, course.getId());

        Mockito.verify(mockRepository, Mockito.times(1))
                .deleteById(course.getId());
    }

    @Test
    public void delete_should_throwException_when_Unauthorized() {
        var user = Helpers.createMockUser();
        var role = Helpers.createMockRole();
        role.setId(2);
        user.setRole(role);
        var course = Helpers.createMockCourse();

        Assertions.assertThrows(UnauthorizedOperationException.class, () -> service.deleteById(user, course.getId()));
    }

    @Test
    public void saveCourse_should_CallRepository() {
        var user = Helpers.createMockUser();
        var role = Helpers.createMockRole();
        role.setId(4);
        user.setRole(role);
        var course = Helpers.createMockCourse();

        service.saveCourse(user, course);

        Mockito.verify(mockRepository, Mockito.times(1))
                .save(course);
    }


    @Test
    public void getCourse_should_callRepository() {
        var course = Helpers.createMockCourse();

        service.getCourse(course.getId());

        Mockito.verify(mockRepository, Mockito.times(1))
                .getCourse(course.getId());
    }

    @Test
    public void getMyCourse_should_callRepository() {
        var user = Helpers.createMockUser();

        service.getMyCourses(user.getId());

        Mockito.verify(mockRepository, Mockito.times(1)).getMyCourses(user.getId());
    }

    @Test
    public void searchAll_shouldCall_repository() {
        Mockito.when(mockRepository.searchAll(Optional.empty()))
                .thenReturn(new ArrayList<>());

        service.searchAll(Optional.empty());

        Mockito.verify(mockRepository, Mockito.times(1)).searchAll(Optional.empty());
    }


}
