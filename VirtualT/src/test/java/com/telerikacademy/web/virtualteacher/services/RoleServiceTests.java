package com.telerikacademy.web.virtualteacher.services;

import com.telerikacademy.web.virtualteacher.Helpers;
import com.telerikacademy.web.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.web.virtualteacher.repositories.RoleRepository;
import com.telerikacademy.web.virtualteacher.services.classes.RoleServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class RoleServiceTests {

    @Mock
    RoleRepository mockRepository;
    @InjectMocks
    RoleServiceImpl service;

    @Test
    public void getAll_should_callRepository() {
        Mockito.when(mockRepository.findAll())
                .thenReturn(new ArrayList<>());

        service.getAll();

        Mockito.verify(mockRepository, Mockito.times(1)).findAll();
    }

    @Test
    public void getById_should_returnRole_when_matchExists() {
        var role = Helpers.createMockRole();

        Mockito.when(mockRepository.findById(role.getId()))
                .thenReturn(Optional.of(role));

        var result = service.getById(role.getId());

        Assertions.assertEquals(role.getName(), result.getName());
    }

    @Test
    public void getById_should_throwException_when_matchDoesntExist() {
        int nonExistingRoleId = 20;

        Assertions.assertThrows(EntityNotFoundException.class, () -> service.getById(nonExistingRoleId));

    }

}
