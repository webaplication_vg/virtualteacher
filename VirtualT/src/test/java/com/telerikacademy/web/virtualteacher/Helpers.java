package com.telerikacademy.web.virtualteacher;

import com.telerikacademy.web.virtualteacher.models.*;

import java.sql.Date;

public class Helpers {
    public static TopicsEntity createMockTopic() {
        var mockTopic = new TopicsEntity();

        mockTopic.setId(1);
        mockTopic.setName("MOCK");

        return mockTopic;
    }

    public static VerificationsEntity createMockVerification() {
        var mockVerification = new VerificationsEntity();
        var user=createMockUser();

        mockVerification.setId(1);
        mockVerification.setUser(user);
        mockVerification.setVerificationCode(69);

        return mockVerification;
    }

    public static UsersCoursesEntity createMockUserCourse() {
        var mockUserCourse = new UsersCoursesEntity();

        var user=createMockUser();
        var course=createMockCourse();

        mockUserCourse.setId(1);
        mockUserCourse.setUser(user);
        mockUserCourse.setCourse(course);

        return mockUserCourse;
    }

    public static RolesEntity createMockRole() {
        var mockRole = new RolesEntity();

        mockRole.setId(1);
        mockRole.setName("Mock Role");

        return mockRole;
    }

    public static GradesEntity createMockGrade() {
        var mockGrade = new GradesEntity();

        mockGrade.setId(1);
        mockGrade.setName("Mock GRADE");

        return mockGrade;
    }

    public static StatusesEntity createMockStatus() {
        var mock = new StatusesEntity();

        mock.setId(1);
        mock.setName("Mock status");

        return mock;
    }

    public static LecturesEntity createMockLecture() {
        var mock = new LecturesEntity();
        var course = createMockCourse();
        mock.setId(1);
        mock.setCourse(course);
        mock.setName("MockName");
        mock.setAssignment("Mock assignment");
        mock.setDescription("Mock description");
        mock.setVideoUrl("no video");

        return mock;
    }

    public static CoursesEntity createMockCourse() {
        var mockStatus = createMockStatus();
        var mockTopic = createMockTopic();
        var mock = new CoursesEntity();

        mock.setId(1);
        mock.setDescription("Mock descr");
        mock.setName("mock name");
        mock.setStatus(mockStatus);
        mock.setStartingDate(Date.valueOf("2020-10-10"));
        mock.setTopic(mockTopic);
        mock.setRating(5);

        return mock;
    }

    public static UsersEntity createMockUser() {
        var mockUser = new UsersEntity();
        var role = Helpers.createMockRole();
        mockUser.setId(1);
        mockUser.setPassword("1234");
        mockUser.setFirstName("user");
        mockUser.setLastName("user");
        mockUser.setRole(role);
        mockUser.setEmail("user@user.com");

        return mockUser;
    }

    public static AssignmentsEntity createMockAssignment() {
        var mockAssignment = new AssignmentsEntity();
        var user = createMockUser();
        var lecture = createMockLecture();
        var grade = createMockGrade();

        mockAssignment.setId(1);
        mockAssignment.setUser(user);
        mockAssignment.setLecture(lecture);
        mockAssignment.setGrade(grade);

        return mockAssignment;
    }

    public static TeacherApplicantsEntity createMockApplication() {
        var mock = new TeacherApplicantsEntity();
        var user = createMockUser();

        mock.setId(1);
        mock.setUser(user);
        return mock;
    }
}
