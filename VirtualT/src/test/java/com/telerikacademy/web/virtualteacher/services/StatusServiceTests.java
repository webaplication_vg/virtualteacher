package com.telerikacademy.web.virtualteacher.services;

import com.telerikacademy.web.virtualteacher.Helpers;
import com.telerikacademy.web.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.web.virtualteacher.repositories.StatusRepository;
import com.telerikacademy.web.virtualteacher.services.classes.StatusServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class StatusServiceTests {
    @Mock
    StatusRepository mockRepository;

    @InjectMocks
    StatusServiceImpl service;

    @Test
    public void getAll_should_callRepository() {
        Mockito.when(mockRepository.findAll())
                .thenReturn(new ArrayList<>());

        service.getAll();

        Mockito.verify(mockRepository, Mockito.times(1))
                .findAll();
    }

    @Test
    public void getById_should_returnTopic_when_matchExists() {
        var status = Helpers.createMockStatus();

        Mockito.when(mockRepository.findById(status.getId()))
                .thenReturn(Optional.of(status));

        var result = service.getById(status.getId());

        Assertions.assertEquals(status.getName(), result.getName());
    }

    @Test
    public void getById_should_throwException_when_matchDoesntExist() {
        int nonExistingStatusId = 2;

        Assertions.assertThrows(EntityNotFoundException.class, () -> service.getById(nonExistingStatusId));

    }
}
