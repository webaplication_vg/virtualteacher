package com.telerikacademy.web.virtualteacher.services;

import com.telerikacademy.web.virtualteacher.Helpers;
import com.telerikacademy.web.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.web.virtualteacher.exceptions.UnauthorizedOperationException;
import com.telerikacademy.web.virtualteacher.repositories.TeacherApplicantRepository;
import com.telerikacademy.web.virtualteacher.services.classes.TeacherApplicantServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class TeacherApplicantServiceTests {
    @Mock
    TeacherApplicantRepository mockRepository;
    @InjectMocks
    TeacherApplicantServiceImpl service;

    @Test
    public void getAll_should_callRepository() {
        Mockito.when(mockRepository.findAll())
                .thenReturn(new ArrayList<>());

        service.getAll();

        Mockito.verify(mockRepository, Mockito.times(1)).findAll();
    }

    @Test
    public void searchAll_shouldCall_repository() {
        var user = Helpers.createMockUser();
        var role = Helpers.createMockRole();
        role.setId(4);
        user.setRole(role);

        Mockito.when(mockRepository.searchAll(Optional.empty()))
                .thenReturn(new ArrayList<>());

        service.searchAll(user, Optional.empty());

        Mockito.verify(mockRepository, Mockito.times(1)).searchAll(Optional.empty());
    }

    @Test
    public void searchAll_should_throwException_when_Unauthorized() {
        var user = Helpers.createMockUser();

        Assertions.assertThrows(UnauthorizedOperationException.class, () -> service.searchAll(user, Optional.empty()));

    }

    @Test
    public void getById_should_callRepository() {
        var app = Helpers.createMockApplication();

        Mockito.when(mockRepository.findById(app.getId()))
                .thenReturn(Optional.of(app));

        var result = service.getById(app.getId());

        Assertions.assertEquals(app.getUser(), result.getUser());
    }

    @Test
    public void getById_should_throwException_when_matchDoesntExist() {
        int nonExistingId = 200;

        Assertions.assertThrows(EntityNotFoundException.class, () -> service.getById(nonExistingId));

    }

    @Test
    public void delete_should_callRepository() {
        var user = Helpers.createMockUser();
        var role = Helpers.createMockRole();
        role.setId(4);
        user.setRole(role);
        var app = Helpers.createMockApplication();

        service.deleteById(user, app.getId());

        Mockito.verify(mockRepository, Mockito.times(1))
                .deleteById(app.getId());
    }

    @Test
    public void delete_should_throwException_when_Unauthorized() {
        var user = Helpers.createMockUser();
        var role = Helpers.createMockRole();
        role.setId(2);
        user.setRole(role);
        var app = Helpers.createMockApplication();

        Assertions.assertThrows(UnauthorizedOperationException.class, () -> service.deleteById(user, app.getId()));
    }

    @Test
    public void saveApplication_should_CallRepository() {
        var app = Helpers.createMockApplication();

        service.saveApplication(app);

        Mockito.verify(mockRepository, Mockito.times(1))
                .save(app);

    }

//    @Test
//    public void saveApplication_should_throwException_ifAlreadySubmittedOnce(){
//        var app = Helpers.createMockApplication();
//
//        Assertions.assertThrows(DuplicateEntityException.class, () -> service.saveApplication(app));
//    }
}
