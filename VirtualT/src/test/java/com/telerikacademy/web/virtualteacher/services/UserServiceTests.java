package com.telerikacademy.web.virtualteacher.services;

import com.telerikacademy.web.virtualteacher.Helpers;
import com.telerikacademy.web.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.web.virtualteacher.exceptions.UnauthorizedOperationException;
import com.telerikacademy.web.virtualteacher.repositories.UserRepository;
import com.telerikacademy.web.virtualteacher.services.classes.UserServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class UserServiceTests {
    @Mock
    UserRepository mockRepository;
    @InjectMocks
    UserServiceImpl service;

    @Test
    public void getAll_should_callRepository() {
        Mockito.when(mockRepository.findAll())
                .thenReturn(new ArrayList<>());

        service.getAll();

        Mockito.verify(mockRepository, Mockito.times(1)).findAll();
    }

    @Test
    public void getAllStudents_should_callRepository() {
        Mockito.when(mockRepository.findAllStudents())
                .thenReturn(new ArrayList<>());

        service.getAllStudents();

        Mockito.verify(mockRepository, Mockito.times(1)).findAllStudents();
    }

    @Test
    public void getAllTeachers_should_callRepository() {
        Mockito.when(mockRepository.findAllTeachers())
                .thenReturn(new ArrayList<>());

        service.getAllTeachers();

        Mockito.verify(mockRepository, Mockito.times(1)).findAllTeachers();
    }

    @Test
    public void getById_should_callRepository() {
        var user = Helpers.createMockUser();

        Mockito.when(mockRepository.findById(user.getId()))
                .thenReturn(Optional.of(user));

        var result = service.getById(user.getId(), user);

        Assertions.assertEquals(user.getEmail(), result.getEmail());
    }

    @Test
    public void getById_should_throwException_when_matchDoesntExist() {
        int nonExistingId = 200;

        var user = Helpers.createMockUser();

        Assertions.assertThrows(UnauthorizedOperationException.class, () -> service.getById(nonExistingId, user));
    }

    @Test
    public void getByEmail_should_callRepository() {
        var user = Helpers.createMockUser();

        Mockito.when(mockRepository.getByEmail(user.getEmail()))
                .thenReturn(user);

        var result = service.getByEmail(user.getEmail());

        Assertions.assertEquals(user.getId(), result.getId());
    }

    @Test
    public void updateUser_should_CallRepository() {
        var user = Helpers.createMockUser();

        service.updateUser(user, user);

        Mockito.verify(mockRepository, Mockito.times(1))
                .save(user);
    }

    @Test
    public void updateUser_should_throwException_when_Unauthorized() {
        var initiator = Helpers.createMockUser();
        var userToUpdate = Helpers.createMockUser();
        userToUpdate.setId(14234234);

        Assertions.assertThrows(UnauthorizedOperationException.class, () -> service.updateUser(initiator, userToUpdate));
    }

    @Test
    public void delete_should_CallRepository() {
        var user = Helpers.createMockUser();
        var authorized = Helpers.createMockUser();
        var role = Helpers.createMockRole();
        role.setId(4);
        authorized.setRole(role);

        service.deleteById(user.getId(), authorized);

        Mockito.verify(mockRepository, Mockito.times(1))
                .deleteById(user.getId());
    }

    @Test
    public void delete_should_throwException_when_Unauthorized() {
        var initiator = Helpers.createMockUser();
        var userToDelete = Helpers.createMockUser();

        Assertions.assertThrows(UnauthorizedOperationException.class, () -> service.deleteById(userToDelete.getId(), initiator));
    }

    @Test
    public void searchAll_shouldCall_repository() {
        var user = Helpers.createMockUser();
        var role = Helpers.createMockRole();
        role.setId(4);
        user.setRole(role);


        Mockito.when(mockRepository.searchAll(Optional.empty()))
                .thenReturn(new ArrayList<>());

        service.searchAll(Optional.empty(), user);

        Mockito.verify(mockRepository, Mockito.times(1)).searchAll(Optional.empty());
    }

    @Test
    public void searchAll_should_throwException_when_Unauthorized() {
        var initiator = Helpers.createMockUser();

        Assertions.assertThrows(UnauthorizedOperationException.class, () -> service.searchAll(Optional.empty(), initiator));
    }


}
