package com.telerikacademy.web.virtualteacher.services;

import com.telerikacademy.web.virtualteacher.Helpers;
import com.telerikacademy.web.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.web.virtualteacher.repositories.TopicRepository;
import com.telerikacademy.web.virtualteacher.services.classes.TopicServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class TopicServiceTests {

    @Mock
    TopicRepository mockRepository;
    @InjectMocks
    TopicServiceImpl service;

    @Test
    public void getAll_should_callRepository() {
        Mockito.when(mockRepository.findAll())
                .thenReturn(new ArrayList<>());

        service.getAll();

        Mockito.verify(mockRepository, Mockito.times(1)).findAll();
    }

    @Test
    public void getById_should_returnTopic_when_matchExists() {
        var topic = Helpers.createMockTopic();

        Mockito.when(mockRepository.findById(topic.getId()))
                .thenReturn(Optional.of(topic));

        var result = service.getById(topic.getId());

        Assertions.assertEquals(topic.getName(), result.getName());
    }

    @Test
    public void getById_should_throwException_when_matchDoesntExist() {
        int nonExistingTopicId = 5;

        Assertions.assertThrows(EntityNotFoundException.class, () -> service.getById(nonExistingTopicId));
    }


    @Test
    public void create_should_callRepository() {
        var topic = Helpers.createMockTopic();

        service.saveTopic(topic);

        Mockito.verify(mockRepository, Mockito.times(1))
                .save(topic);
    }
}
