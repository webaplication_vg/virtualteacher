package com.telerikacademy.web.virtualteacher.services;

import com.telerikacademy.web.virtualteacher.Helpers;
import com.telerikacademy.web.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.web.virtualteacher.exceptions.UnauthorizedOperationException;
import com.telerikacademy.web.virtualteacher.repositories.AssignmentRepository;
import com.telerikacademy.web.virtualteacher.services.classes.AssignmentServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class AssignmentServiceTests {
    @Mock
    AssignmentRepository mockRepository;

    @InjectMocks
    AssignmentServiceImpl service;

    @Test
    public void getAll_should_callRepository() {
        Mockito.when(mockRepository.findAll())
                .thenReturn(new ArrayList<>());

        service.getAll();

        Mockito.verify(mockRepository, Mockito.times(1)).findAll();
    }

    @Test
    public void getAllByUserId_should_callRepository() {
        var user = Helpers.createMockUser();
        Mockito.when(mockRepository.getAllByUserId(user.getId()))
                .thenReturn(new ArrayList<>());

        service.getAllByUserId(user.getId());

        Mockito.verify(mockRepository, Mockito.times(1)).getAllByUserId(user.getId());
    }

    @Test
    public void getByUserIdAndLectureId_should_callRepository() {
        var user = Helpers.createMockUser();
        var lecture = Helpers.createMockLecture();
        Mockito.when(mockRepository.getByUserIdAndLectureId(user.getId(), lecture.getId()))
                .thenReturn(new ArrayList<>());

        service.getByUserIdAndLectureId(user.getId(), lecture.getId());

        Mockito.verify(mockRepository, Mockito.times(1)).getByUserIdAndLectureId(user.getId(), lecture.getId());

    }

    @Test
    public void getById_should_callRepository() {
        var assignment = Helpers.createMockAssignment();

        Mockito.when(mockRepository.findById(assignment.getId()))
                .thenReturn(Optional.of(assignment));

        var result = service.getById(assignment.getId());

        Assertions.assertEquals(assignment.getUser(), result.getUser());
    }

    @Test
    public void getById_should_throwException_when_matchDoesntExist() {
        int nonExistingId = 200;

        Assertions.assertThrows(EntityNotFoundException.class, () -> service.getById(nonExistingId));

    }

    @Test
    public void saveAssignment_should_CallRepository() {
        var assignment = Helpers.createMockAssignment();
        var user = Helpers.createMockUser();
        var role = Helpers.createMockRole();
        role.setId(4);
        user.setRole(role);

        service.saveAssignment(user, assignment);

        Mockito.verify(mockRepository, Mockito.times(1))
                .save(assignment);
    }

    @Test
    public void saveAssignment_should_throwException_when_Unauthorized() {
        var user = Helpers.createMockUser();
        var role = Helpers.createMockRole();
        role.setId(1);
        user.setRole(role);
        var assignment = Helpers.createMockAssignment();

        Assertions.assertThrows(UnauthorizedOperationException.class, () -> service.saveAssignment(user, assignment));
    }
}
