package com.telerikacademy.web.virtualteacher.services;

import com.telerikacademy.web.virtualteacher.Helpers;
import com.telerikacademy.web.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.web.virtualteacher.repositories.UserCourseRepository;
import com.telerikacademy.web.virtualteacher.services.classes.UserCourseServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Optional;

@ExtendWith({MockitoExtension.class})
public class UserCourseServiceTests {

    @Mock
    UserCourseRepository mockRepository;

    @InjectMocks
    UserCourseServiceImpl service;


    @Test
    public void getAll_should_callRepository() {
        Mockito.when(mockRepository.findAll())
                .thenReturn(new ArrayList<>());

        service.getAll();

        Mockito.verify(mockRepository, Mockito.times(1)).findAll();
    }

    @Test
    public void getById_should_callRepository() {
        var userCourse = Helpers.createMockUserCourse();

        Mockito.when(mockRepository.findById(userCourse.getId()))
                .thenReturn(Optional.of(userCourse));

        var result = service.getById(userCourse.getId());

        Assertions.assertEquals(userCourse.getCourse(), result.getCourse());
    }

    @Test
    public void getById_should_throwException_when_matchDoesntExist() {
        int nonExistingId = 200;

        Assertions.assertThrows(EntityNotFoundException.class, () -> service.getById(nonExistingId));
    }

    @Test
    public void getByCourseId_should_callRepository() {
        var userCourse = Helpers.createMockUserCourse();

        Mockito.when(mockRepository.getByCourseId(userCourse.getCourse().getId(), userCourse.getUser().getId()))
                .thenReturn(userCourse);

        var result = service.getByCourseId(userCourse.getCourse().getId(), userCourse.getUser());

        Assertions.assertEquals(userCourse.getCourse(), result.getCourse());
    }

    @Test
    public void create_should_callRepository() {
        var userCourse = Helpers.createMockUserCourse();

        service.saveUserCourse(userCourse);

        Mockito.verify(mockRepository, Mockito.times(1))
                .save(userCourse);
    }

    @Test
    public void searchAll_shouldCall_repository() {
        var course = Helpers.createMockCourse();

        Mockito.when(mockRepository.searchAll(Optional.empty(), course.getId()))
                .thenReturn(new ArrayList<>());

        service.searchAll(Optional.empty(), course.getId());

        Mockito.verify(mockRepository, Mockito.times(1)).searchAll(Optional.empty(), course.getId());
    }

    @Test
    public void getCourseSumRating_shouldCall_repository() {
        var course = Helpers.createMockCourse();

        Mockito.when(mockRepository.getCourseSumRating(course.getId()))
                .thenReturn(course.getRating());

        var result = service.getCourseSumRating(course.getId());

        Assertions.assertEquals(course.getRating(), result);

    }

    @Test
    public void getUserCountByCourseId_shouldCall_repository() {
        var userCourse = Helpers.createMockUserCourse();
        var userCount = 1.0;

        Mockito.when(mockRepository.getUserCountByCourseId(userCourse.getCourse().getId()))
                .thenReturn(userCount);

        var result = service.getUserCountByCourseId(userCourse.getCourse().getId());

        Assertions.assertEquals(userCount, result);

    }


}
