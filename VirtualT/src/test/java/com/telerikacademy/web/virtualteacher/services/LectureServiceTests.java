package com.telerikacademy.web.virtualteacher.services;

import com.telerikacademy.web.virtualteacher.Helpers;
import com.telerikacademy.web.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.web.virtualteacher.exceptions.UnauthorizedOperationException;
import com.telerikacademy.web.virtualteacher.repositories.LectureRepository;
import com.telerikacademy.web.virtualteacher.services.classes.LectureServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class LectureServiceTests {
    @Mock
    LectureRepository mockRepository;
    @InjectMocks
    LectureServiceImpl service;

    @Test
    public void getAll_should_callRepository() {
        Mockito.when(mockRepository.findAll())
                .thenReturn(new ArrayList<>());

        service.getAll();

        Mockito.verify(mockRepository, Mockito.times(1)).findAll();
    }

    @Test
    public void getById_should_returnTopic_when_matchExists() {
        var lecture = Helpers.createMockLecture();

        Mockito.when(mockRepository.findById(lecture.getId()))
                .thenReturn(Optional.of(lecture));

        var result = service.getById(lecture.getId());

        Assertions.assertEquals(lecture.getName(), result.getName());
    }

    @Test
    public void getById_should_throwException_when_matchDoesntExist() {
        int nonExistingLectureId = 200;

        Assertions.assertThrows(EntityNotFoundException.class, () -> service.getById(nonExistingLectureId));

    }

    @Test
    public void searchAll_shouldCall_repository() {
        Mockito.when(mockRepository.searchAll(Optional.empty(), 20))
                .thenReturn(new ArrayList<>());

        service.searchAll(Optional.empty(), 20);

        Mockito.verify(mockRepository, Mockito.times(1)).searchAll(Optional.empty(), 20);
    }

    @Test
    public void saveLecture_should_CallRepository() {
        var user = Helpers.createMockUser();
        var role = Helpers.createMockRole();
        role.setId(4);
        user.setRole(role);
        var lecture = Helpers.createMockLecture();

        service.saveLecture(user, lecture);

        Mockito.verify(mockRepository, Mockito.times(1))
                .save(lecture);
    }

    @Test
    public void saveLecture_should_throwException_when_Unauthorized() {
        var user = Helpers.createMockUser();
        var role = Helpers.createMockRole();
        role.setId(2);
        user.setRole(role);
        var lecture = Helpers.createMockLecture();

        Assertions.assertThrows(UnauthorizedOperationException.class, () -> service.saveLecture(user, lecture));
    }

    @Test
    public void delete_should_callRepository() {
        var user = Helpers.createMockUser();
        var role = Helpers.createMockRole();
        role.setId(4);
        user.setRole(role);
        var lecture = Helpers.createMockLecture();

        service.deleteById(user, lecture.getId());

        Mockito.verify(mockRepository, Mockito.times(1))
                .deleteById(lecture.getId());
    }

    @Test
    public void delete_should_throwException_when_Unauthorized() {
        var user = Helpers.createMockUser();
        var role = Helpers.createMockRole();
        role.setId(2);
        user.setRole(role);
        var lecture = Helpers.createMockLecture();

        Assertions.assertThrows(UnauthorizedOperationException.class, () -> service.deleteById(user, lecture.getId()));
    }

}
