package com.telerikacademy.web.virtualteacher.services;

import com.telerikacademy.web.virtualteacher.Helpers;
import com.telerikacademy.web.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.web.virtualteacher.exceptions.UnauthorizedOperationException;
import com.telerikacademy.web.virtualteacher.models.VerificationsEntity;
import com.telerikacademy.web.virtualteacher.repositories.LectureRepository;
import com.telerikacademy.web.virtualteacher.repositories.VerificationRepository;
import com.telerikacademy.web.virtualteacher.services.classes.LectureServiceImpl;
import com.telerikacademy.web.virtualteacher.services.classes.VerificationServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class VerificationServiceTests {
    @Mock
    VerificationRepository mockRepository;
    @InjectMocks
    VerificationServiceImpl service;


    @Test
    public void saveVerification_should_CallRepository() {
        var verification = Helpers.createMockVerification();

        service.create(verification);

        Mockito.verify(mockRepository, Mockito.times(1))
                .save(verification);
    }

    @Test
    public void getByData_should_CallRepository() {
        var verification = Helpers.createMockVerification();
        List<VerificationsEntity> verificationsEntities=new ArrayList<>();
        verificationsEntities.add(verification);

        Mockito.when(mockRepository.getByData(verification.getUser().getEmail(),verification.getUser().getPassword(),verification.getVerificationCode())).
                thenReturn(verificationsEntities);

        var result = service.getByData(verification.getUser().getEmail(),verification.getUser().getPassword(),verification.getVerificationCode());

        Assertions.assertEquals(verification,result);
    }



}
