# VirtualTeacher



The application is a platform that makes studying easy and fun.
VirtualTeacher's students have access to various courses and lectures,
also they have the opportunity to become teachers themselves.


The project has Swagger documentation which can be accessed by the following link
http://localhost:8080/swagger-ui/index.html#/


In the DB_scripts folder you can find database scripts and create the database for this project. It's relational database constructed with MariaDB RDBMS

![](./Pictures/DB_tables.png)