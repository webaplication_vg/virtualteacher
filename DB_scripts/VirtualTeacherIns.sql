-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.6.2-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for virtualteacher
CREATE DATABASE IF NOT EXISTS `virtualteacher` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `virtualteacher`;

-- Dumping structure for table virtualteacher.assignments
CREATE TABLE IF NOT EXISTS `assignments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `lecture_id` int(11) DEFAULT NULL,
  `file` varchar(100) DEFAULT NULL,
  `grade_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `assignments_id_uindex` (`id`),
  KEY `assignments_users_fk` (`user_id`),
  KEY `assignments_lectures_fk` (`lecture_id`),
  KEY `assignments_grades_fk` (`grade_id`),
  CONSTRAINT `assignments_grades_fk` FOREIGN KEY (`grade_id`) REFERENCES `grades` (`id`),
  CONSTRAINT `assignments_lectures_fk` FOREIGN KEY (`lecture_id`) REFERENCES `lectures` (`id`) ON DELETE CASCADE,
  CONSTRAINT `assignments_users_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=latin1;

-- Dumping data for table virtualteacher.assignments: ~2 rows (approximately)
/*!40000 ALTER TABLE `assignments` DISABLE KEYS */;
INSERT INTO `assignments` (`id`, `user_id`, `lecture_id`, `file`, `grade_id`) VALUES
	(51, 23, 41, 'database.txt', 5),
	(52, 28, 60, 'proekt.txt', 6);
/*!40000 ALTER TABLE `assignments` ENABLE KEYS */;

-- Dumping structure for table virtualteacher.courses
CREATE TABLE IF NOT EXISTS `courses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL,
  `picture` varchar(500) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `starting_date` date DEFAULT NULL,
  `topic_id` int(11) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  `rating` double(11,0) DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `courses_id_uindex` (`id`),
  KEY `courses_topic_fk` (`topic_id`),
  KEY `courses_statuses_fk` (`status_id`),
  CONSTRAINT `courses_statuses_fk` FOREIGN KEY (`status_id`) REFERENCES `statuses` (`id`),
  CONSTRAINT `courses_topic_fk` FOREIGN KEY (`topic_id`) REFERENCES `topics` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;

-- Dumping data for table virtualteacher.courses: ~6 rows (approximately)
/*!40000 ALTER TABLE `courses` DISABLE KEYS */;
INSERT INTO `courses` (`id`, `name`, `picture`, `description`, `starting_date`, `topic_id`, `status_id`, `rating`) VALUES
	(27, 'Java for beginners', 'https://camo.githubusercontent.com/537ff5237f38eda1091ba7221dde258733ac6de30a36fbda5be8d3c4364eba1a/68747470733a2f2f63646e2e766f782d63646e2e636f6d2f7468756d626f722f5f416f625a5a44745f525653746b745652376d555a70426b6f76633d2f3078303a363430783432372f31323030783830302f66696c746572733a666f63616c283078303a36343078343237292f63646e2e766f782d63646e2e636f6d2f6173736574732f313038373133372f6a6176615f6c6f676f5f3634302e6a7067', 'Learn Java In This Course And Become a Computer Programmer. Obtain valuable Core Java Skills And Java Certification', '2021-09-14', 1, 2, 4),
	(28, 'C# for beginners', 'https://static.gunnarpeipman.com/wp-content/uploads/2009/10/csharp-featured.png', 'Learn C# In This Course And Become a Computer Programmer. Obtain valuable Core C# Skills And C# Certification', '2021-09-16', 4, 1, 0),
	(29, 'Python for beginners', 'https://www.virtualbeehive.com/wp-content/uploads/2019/08/logo-python.png', 'Learn Python In This Course And Become a Computer Programmer. Obtain valuable Core Python Skills And Python Certification', '2021-09-15', 3, 2, 3),
	(30, 'Java Intermediate', 'https://www.eginnovations.com/blog/wp-content/uploads/2019/06/how-to-troubleshoot-java-cpu.jpg', 'Master your language with lessons, quizzes, and projects designed for real-life scenarios. Create portfolio projects that showcase your new skills to help land your dream job.', '2021-10-21', 1, 2, 0),
	(31, 'C# intermediate', 'https://www.syncfusion.com/blogs/wp-content/uploads/2019/11/csharp-using-declaration.png', 'Master your language with lessons, quizzes, and projects designed for real-life scenarios. Create portfolio projects that showcase your new skills to help land your dream job.', '2021-10-19', 4, 2, 0),
	(32, 'Python Intermediate', 'https://miro.medium.com/max/1400/1*ueWmI48uuShON-hX7LwI0w.png', 'Master your language with lessons, quizzes, and projects designed for real-life scenarios. Create portfolio projects that showcase your new skills to help land your dream job.', '2021-10-20', 3, 2, 0);
/*!40000 ALTER TABLE `courses` ENABLE KEYS */;

-- Dumping structure for table virtualteacher.grades
CREATE TABLE IF NOT EXISTS `grades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `grades_id_uindex` (`id`),
  UNIQUE KEY `grades_name_uindex` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumping data for table virtualteacher.grades: ~5 rows (approximately)
/*!40000 ALTER TABLE `grades` DISABLE KEYS */;
INSERT INTO `grades` (`id`, `name`) VALUES
	(6, 'Excellent'),
	(4, 'Good'),
	(1, 'Not Graded'),
	(2, 'Poor'),
	(3, 'Satisfactory'),
	(5, 'Very Good');
/*!40000 ALTER TABLE `grades` ENABLE KEYS */;

-- Dumping structure for table virtualteacher.lectures
CREATE TABLE IF NOT EXISTS `lectures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `assignment` varchar(500) DEFAULT NULL,
  `video_url` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lectures_id_uindex` (`id`),
  KEY `lectures_courses_fk` (`course_id`),
  CONSTRAINT `lectures_courses_fk` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=latin1;

-- Dumping data for table virtualteacher.lectures: ~35 rows (approximately)
/*!40000 ALTER TABLE `lectures` DISABLE KEYS */;
INSERT INTO `lectures` (`id`, `name`, `course_id`, `description`, `assignment`, `video_url`) VALUES
	(34, 'Introduction to Java Programming', 27, 'Java Programming: Introduction to Java Programming\r\nTopics discussed:\r\n1. About Java.\r\n2. Java Language Specification.\r\n3. API.\r\n4. Editions of Java.\r\n5. IDE.', 'What is Java?\r\n', 'https://www.youtube.com/embed/mG4NLNZ37y4'),
	(35, 'Anatomy of Java Program', 27, 'Topics discussed:Java Programming: The Anatomy of Java Program\r\nTopics discussed:\r\n1. Classes & Objects.\r\n2. Methods.\r\n3. Naming Conventions.\r\n4. Java Program Structure.\r\n5. Packages.', 'What are: 1. Classes & Objects.\r\n2. Methods.\r\n3. Naming Conventions.\r\n4. Java Program Structure.\r\n5. Packages.', 'https://www.youtube.com/embed/vsxYucdzimA'),
	(36, 'Displaying Messages in Java', 27, 'Java Programming: Displaying Messages in Java\r\nTopics discussed:\r\n1. Strings in Java.\r\n2. println() & print() methods in Java.\r\n3. System Class in Java.', 'What is:\r\n1. Strings in Java.\r\n2. println() & print() methods in Java.\r\n3. System Class in Java.', 'https://www.youtube.com/embed/ifirpBZLeCk'),
	(37, 'Displaying Numbers in Java', 27, 'Java Programming: Displaying Numbers in Java\r\nTopics discussed:\r\n1. Numbers in Java.\r\n2. Some Arithmetic Operators.\r\n3. Printing some values.', 'What did you learn?', 'https://www.youtube.com/embed/UFMqdnUh4nI'),
	(38, 'Configuring our Java Development Environment', 27, 'Java Programming: Configuring the Java Development Environment\r\nTopics discussed:\r\n1. Install JDK 13.0.1\r\n2. Setting up the environment variables.\r\n3. Install IntelliJ IDEA 2019.2.4', 'What did you learn?', 'https://www.youtube.com/embed/FjGMYpXS9iE'),
	(39, 'Creating & Executing a Java Program', 27, 'Java Programming: Creating, Compiling, and Executing a Java Program\r\nTopics discussed:\r\n1. Creating a Java program using a simple text editor.\r\n2. Compiling the Java program using the Command Window.\r\n3. Running the Java program using the Command Window.\r\n4. What happens exactly when we compile and run our Java program.', 'What did you learn?', 'https://www.youtube.com/embed/gHXzyAkbUhk'),
	(40, 'Our First Java Project', 27, 'Java Programming: Our First Java Project \r\nTopics discussed:\r\n1. Comments in Java.\r\n2. Creating our first Java project in IntelliJ.\r\n3. Writing the Java program in IntelliJ.\r\n4. Running the Java program in IntelliJ.', 'What did you learn?', 'https://www.youtube.com/embed/AVpLMoTnwM8'),
	(41, 'C# Programming Tutorial 1 - Intro', 28, 'C# (pronounced "See Sharp") is a modern, object-oriented, and type-safe programming language. ... C# is an object-oriented, component-oriented programming language. C# provides language constructs to directly support these concepts, making C# a natural language in which to create and use software components.', 'What did you learn?', 'https://www.youtube.com/embed/4vQJMyk2DPU'),
	(42, 'C# Programming Tutorial 2 - Hello World', 28, 'C# (pronounced "See Sharp") is a modern, object-oriented, and type-safe programming language. ... C# is an object-oriented, component-oriented programming language. C# provides language constructs to directly support these concepts, making C# a natural language in which to create and use software components.', 'What did you learn?', 'https://www.youtube.com/embed/eXtW87Wi3Bw'),
	(43, 'C# Programming Tutorial 3 - Application Architectu', 28, 'C# (pronounced "See Sharp") is a modern, object-oriented, and type-safe programming language. ... C# is an object-oriented, component-oriented programming language. C# provides language constructs to directly support these concepts, making C# a natural language in which to create and use software components.', 'What did you learn?', 'https://www.youtube.com/embed/l9yOMPx3QSE'),
	(44, 'C# Programming Tutorial 4 - Methods', 28, 'C# (pronounced "See Sharp") is a modern, object-oriented, and type-safe programming language. ... C# is an object-oriented, component-oriented programming language. C# provides language constructs to directly support these concepts, making C# a natural language in which to create and use software components.', 'What did you learn?', 'https://www.youtube.com/embed/6G2NbfadwRA'),
	(45, 'C# Programming Tutorial 5 - Command Line Arguments', 28, 'C# (pronounced "See Sharp") is a modern, object-oriented, and type-safe programming language. ... C# is an object-oriented, component-oriented programming language. C# provides language constructs to directly support these concepts, making C# a natural language in which to create and use software components.', 'What did you learn?', 'https://www.youtube.com/embed/Enocgj5T6y0'),
	(47, 'C# Programming Tutorial 1 - Intro', 31, 'C# (pronounced "See Sharp") is a modern, object-oriented, and type-safe programming language. ... C# is an object-oriented, component-oriented programming language. C# provides language constructs to directly support these concepts, making C# a natural language in which to create and use software components.', 'What did you learn?', 'https://www.youtube.com/embed/4vQJMyk2DPU'),
	(48, 'C# Programming Tutorial 2 - Hello World', 31, 'C# (pronounced "See Sharp") is a modern, object-oriented, and type-safe programming language. ... C# is an object-oriented, component-oriented programming language. C# provides language constructs to directly support these concepts, making C# a natural language in which to create and use software components.', 'What did you learn?', 'https://www.youtube.com/embed/eXtW87Wi3Bw'),
	(49, 'C# Programming Tutorial 3 - Application Architectu', 31, 'C# (pronounced "See Sharp") is a modern, object-oriented, and type-safe programming language. ... C# is an object-oriented, component-oriented programming language. C# provides language constructs to directly support these concepts, making C# a natural language in which to create and use software components.', 'What did you learn?', 'https://www.youtube.com/embed/l9yOMPx3QSE'),
	(50, 'C# Programming Tutorial 4 - Methods', 31, 'C# (pronounced "See Sharp") is a modern, object-oriented, and type-safe programming language. ... C# is an object-oriented, component-oriented programming language. C# provides language constructs to directly support these concepts, making C# a natural language in which to create and use software components.', 'What did you learn?', 'https://www.youtube.com/embed/6G2NbfadwRA'),
	(51, 'C# Programming Tutorial 5 - Command Line Arguments', 31, 'C# (pronounced "See Sharp") is a modern, object-oriented, and type-safe programming language. ... C# is an object-oriented, component-oriented programming language. C# provides language constructs to directly support these concepts, making C# a natural language in which to create and use software components.', 'What did you learn?', 'https://www.youtube.com/embed/Enocgj5T6y0'),
	(52, 'C# Programming Tutorial 6 - User Input', 31, 'C# (pronounced "See Sharp") is a modern, object-oriented, and type-safe programming language. ... C# is an object-oriented, component-oriented programming language. C# provides language constructs to directly support these concepts, making C# a natural language in which to create and use software components.', 'What did you learn?', 'https://www.youtube.com/embed/K4RZKJ50fEw'),
	(53, 'Introduction to Java Programming', 30, 'Java Programming: Introduction to Java Programming\r\nTopics discussed:\r\n1. About Java.\r\n2. Java Language Specification.\r\n3. API.\r\n4. Editions of Java.\r\n5. IDE.', 'What is Java?\r\n', 'https://www.youtube.com/embed/mG4NLNZ37y4'),
	(54, 'Anatomy of Java Program', 30, 'Topics discussed:Java Programming: The Anatomy of Java Program\r\nTopics discussed:\r\n1. Classes & Objects.\r\n2. Methods.\r\n3. Naming Conventions.\r\n4. Java Program Structure.\r\n5. Packages.', 'What are: 1. Classes & Objects.\r\n2. Methods.\r\n3. Naming Conventions.\r\n4. Java Program Structure.\r\n5. Packages.', 'https://www.youtube.com/embed/vsxYucdzimA'),
	(55, 'Displaying Messages in Java', 30, 'Java Programming: Displaying Messages in Java\r\nTopics discussed:\r\n1. Strings in Java.\r\n2. println() & print() methods in Java.\r\n3. System Class in Java.', 'What is:\r\n1. Strings in Java.\r\n2. println() & print() methods in Java.\r\n3. System Class in Java.', 'https://www.youtube.com/embed/ifirpBZLeCk'),
	(56, 'Displaying Numbers in Java', 30, 'Java Programming: Displaying Numbers in Java\r\nTopics discussed:\r\n1. Numbers in Java.\r\n2. Some Arithmetic Operators.\r\n3. Printing some values.', 'What did you learn?', 'https://www.youtube.com/embed/UFMqdnUh4nI'),
	(57, 'Configuring our Java Development Environment', 30, 'Java Programming: Configuring the Java Development Environment\r\nTopics discussed:\r\n1. Install JDK 13.0.1\r\n2. Setting up the environment variables.\r\n3. Install IntelliJ IDEA 2019.2.4', 'What did you learn?', 'https://www.youtube.com/embed/FjGMYpXS9iE'),
	(58, 'Creating & Executing a Java Program', 30, 'Java Programming: Creating, Compiling, and Executing a Java Program\r\nTopics discussed:\r\n1. Creating a Java program using a simple text editor.\r\n2. Compiling the Java program using the Command Window.\r\n3. Running the Java program using the Command Window.\r\n4. What happens exactly when we compile and run our Java program.', 'What did you learn?', 'https://www.youtube.com/embed/gHXzyAkbUhk'),
	(59, 'Our First Java Project', 30, 'Java Programming: Our First Java Project \r\nTopics discussed:\r\n1. Comments in Java.\r\n2. Creating our first Java project in IntelliJ.\r\n3. Writing the Java program in IntelliJ.\r\n4. Running the Java program in IntelliJ.', 'What did you learn?', 'https://www.youtube.com/embed/AVpLMoTnwM8'),
	(60, 'Python Tutorial for Beginners 1: Install and Setup', 29, 'Python is an interpreted, object-oriented, high-level programming language with dynamic semantics. ... Python\'s simple, easy to learn syntax emphasizes readability and therefore reduces the cost of program maintenance. Python supports modules and packages, which encourages program modularity and code reuse.', 'What did you learn?', 'https://www.youtube.com/embed/YYXdXT2l-Gg'),
	(61, 'Python Tutorial for Beginners 2: Strings - Working', 29, 'Python is an interpreted, object-oriented, high-level programming language with dynamic semantics. ... Python\'s simple, easy to learn syntax emphasizes readability and therefore reduces the cost of program maintenance. Python supports modules and packages, which encourages program modularity and code reuse.', 'What did you learn?', 'https://www.youtube.com/embed/k9TUPpGqYTo'),
	(62, 'Python Tutorial for Beginners 3: Integers and Floa', 29, 'Python is an interpreted, object-oriented, high-level programming language with dynamic semantics. ... Python\'s simple, easy to learn syntax emphasizes readability and therefore reduces the cost of program maintenance. Python supports modules and packages, which encourages program modularity and code reuse.', 'What did you learn?', 'https://www.youtube.com/embed/khKv-8q7YmY'),
	(63, 'Python Tutorial for Beginners 4: Lists and Sets', 29, 'Python is an interpreted, object-oriented, high-level programming language with dynamic semantics. ... Python\'s simple, easy to learn syntax emphasizes readability and therefore reduces the cost of program maintenance. Python supports modules and packages, which encourages program modularity and code reuse.', 'What did you learn?', 'https://www.youtube.com/embed/W8KRzm-HUcc'),
	(64, 'Python Tutorial for Beginners 5: Dictionaries - Wo', 29, 'Python is an interpreted, object-oriented, high-level programming language with dynamic semantics. ... Python\'s simple, easy to learn syntax emphasizes readability and therefore reduces the cost of program maintenance. Python supports modules and packages, which encourages program modularity and code reuse.', 'What did you learn?', 'https://www.youtube.com/embed/daefaLgNkw0'),
	(65, 'Python Tutorial for Beginners 1: Install and Setup', 32, 'Python is an interpreted, object-oriented, high-level programming language with dynamic semantics. ... Python\'s simple, easy to learn syntax emphasizes readability and therefore reduces the cost of program maintenance. Python supports modules and packages, which encourages program modularity and code reuse.', 'What did you learn?', 'https://www.youtube.com/embed/YYXdXT2l-Gg'),
	(66, 'Python Tutorial for Beginners 2: Strings - Working', 32, 'Python is an interpreted, object-oriented, high-level programming language with dynamic semantics. ... Python\'s simple, easy to learn syntax emphasizes readability and therefore reduces the cost of program maintenance. Python supports modules and packages, which encourages program modularity and code reuse.', 'What did you learn?', 'https://www.youtube.com/embed/k9TUPpGqYTo'),
	(67, 'Python Tutorial for Beginners 3: Integers and Floa', 32, 'Python is an interpreted, object-oriented, high-level programming language with dynamic semantics. ... Python\'s simple, easy to learn syntax emphasizes readability and therefore reduces the cost of program maintenance. Python supports modules and packages, which encourages program modularity and code reuse.', 'What did you learn?', 'https://www.youtube.com/embed/khKv-8q7YmY'),
	(68, 'Python Tutorial for Beginners 4: Lists and Sets', 32, 'Python is an interpreted, object-oriented, high-level programming language with dynamic semantics. ... Python\'s simple, easy to learn syntax emphasizes readability and therefore reduces the cost of program maintenance. Python supports modules and packages, which encourages program modularity and code reuse.', 'What did you learn?', 'https://www.youtube.com/embed/W8KRzm-HUcc'),
	(69, 'Python Tutorial for Beginners 5: Dictionaries - Wo', 32, 'Python is an interpreted, object-oriented, high-level programming language with dynamic semantics. ... Python\'s simple, easy to learn syntax emphasizes readability and therefore reduces the cost of program maintenance. Python supports modules and packages, which encourages program modularity and code reuse.', 'What did you learn?', 'https://www.youtube.com/embed/daefaLgNkw0');
/*!40000 ALTER TABLE `lectures` ENABLE KEYS */;

-- Dumping structure for table virtualteacher.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table virtualteacher.roles: ~4 rows (approximately)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`id`, `name`) VALUES
	(1, 'Not Verified'),
	(2, 'Student'),
	(3, 'Teacher'),
	(4, 'Admin');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Dumping structure for table virtualteacher.statuses
CREATE TABLE IF NOT EXISTS `statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `statuses_id_uindex` (`id`),
  UNIQUE KEY `statuses_name_uindex` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table virtualteacher.statuses: ~2 rows (approximately)
/*!40000 ALTER TABLE `statuses` DISABLE KEYS */;
INSERT INTO `statuses` (`id`, `name`) VALUES
	(1, 'Hidden'),
	(2, 'Visible');
/*!40000 ALTER TABLE `statuses` ENABLE KEYS */;

-- Dumping structure for table virtualteacher.teacher_applicants
CREATE TABLE IF NOT EXISTS `teacher_applicants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `description` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `teacher_applicants_user_id_uindex` (`user_id`),
  UNIQUE KEY `teacher_applicants_id_uindex` (`id`),
  CONSTRAINT `teacher_applicants_users_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

-- Dumping data for table virtualteacher.teacher_applicants: ~2 rows (approximately)
/*!40000 ALTER TABLE `teacher_applicants` DISABLE KEYS */;
INSERT INTO `teacher_applicants` (`id`, `user_id`, `description`) VALUES
	(19, 23, 'asd'),
	(20, 28, 'iskam da sam o4itel');
/*!40000 ALTER TABLE `teacher_applicants` ENABLE KEYS */;

-- Dumping structure for table virtualteacher.topics
CREATE TABLE IF NOT EXISTS `topics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `topics_name_uindex` (`name`),
  UNIQUE KEY `topics_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Dumping data for table virtualteacher.topics: ~3 rows (approximately)
/*!40000 ALTER TABLE `topics` DISABLE KEYS */;
INSERT INTO `topics` (`id`, `name`) VALUES
	(4, 'C#'),
	(1, 'Java'),
	(3, 'Python');
/*!40000 ALTER TABLE `topics` ENABLE KEYS */;

-- Dumping structure for table virtualteacher.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `profile_picture` varchar(500) DEFAULT NULL,
  `first_name` varchar(30) DEFAULT NULL,
  `last_name` varchar(30) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `password` varchar(30) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_id_uindex` (`id`),
  UNIQUE KEY `users_email_uindex` (`email`),
  KEY `users_roles_fk` (`role_id`),
  CONSTRAINT `users_roles_fk` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

-- Dumping data for table virtualteacher.users: ~14 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `profile_picture`, `first_name`, `last_name`, `email`, `password`, `role_id`) VALUES
	(8, 'default.png', 'Robert', 'Smith', 'robert@gmail.com', '123', 2),
	(10, 'profile-4.jpeg', 'John', 'Johnson', 'john@gmail.com', '123', 2),
	(11, 'video0_31.mp4', 'Michael', 'Williams', 'michael@gmail.com', '123', 2),
	(12, 'default.png', 'David', 'Brown', 'david@gmail.com', '123', 2),
	(13, 'default.png', 'William', 'Jones', 'william@gmail.com', '123', 2),
	(14, 'default.png', 'Richard', 'Garcia', 'richard@gmail.com', '123', 2),
	(17, 'default.png', 'Joseph', 'Miller', 'joseph@gmail.com', '123', 2),
	(18, 'default.png', 'Thomas', 'Davis', 'thomas@gmail.com', '123', 2),
	(23, 'download.jpg', 'Christopher', 'Jackson', 'christopher@gmail.com', '123', 2),
	(24, 'download.jpg', 'Daniel', 'Wilson', 'daniel@gmail.com', '123', 3),
	(25, 'download.jpg', 'Mary', 'Gonzalez', 'mary@gmail.com', '123', 2),
	(26, 'download.jpg', 'Patriciaa', 'Lopezz', 'patricia@gmail.com', '123', 2),
	(27, 'banner.jpg', 'Valentin', 'Petkov', 'valllitoui@gmail.com', '123', 4),
	(28, 'default.png', 'Valentin', 'Petkov', 'valllitomi@gmail.com', '123', 2);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table virtualteacher.users_courses
CREATE TABLE IF NOT EXISTS `users_courses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  `course_rating` int(11) DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_courses_id_uindex` (`id`),
  KEY `users_courses_courses_fk` (`course_id`),
  KEY `users_courses_users_fk` (`user_id`),
  CONSTRAINT `users_courses_courses_fk` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`) ON DELETE CASCADE,
  CONSTRAINT `users_courses_users_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;

-- Dumping data for table virtualteacher.users_courses: ~2 rows (approximately)
/*!40000 ALTER TABLE `users_courses` DISABLE KEYS */;
INSERT INTO `users_courses` (`id`, `user_id`, `course_id`, `course_rating`) VALUES
	(44, 26, 27, 4),
	(45, 26, 29, 3);
/*!40000 ALTER TABLE `users_courses` ENABLE KEYS */;

-- Dumping structure for table virtualteacher.verifications
CREATE TABLE IF NOT EXISTS `verifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `verification_code` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `verifications_id_uindex` (`id`),
  KEY `verifications_users_fk` (`user_id`),
  CONSTRAINT `verifications_users_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- Dumping data for table virtualteacher.verifications: ~1 rows (approximately)
/*!40000 ALTER TABLE `verifications` DISABLE KEYS */;
INSERT INTO `verifications` (`id`, `user_id`, `verification_code`) VALUES
	(15, 28, 55189);
/*!40000 ALTER TABLE `verifications` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
